//
// Created by root on 03.06.2020.
//

#ifndef LIBRARY_GS_AUTH_2_GSA2SOELOGGER_H
#define LIBRARY_GS_AUTH_2_GSA2SOELOGGER_H

#include "gsa2main.h"

G_BEGIN_DECLS

#define GSA2_TYPE_SOE_LOGGER gsa2_soe_logger_get_type()

GE_DECLARE_DERIVABLE_TYPE(GSA2SOELogger, gsa2_soe_logger, GSA2, SOE_LOGGER, SOELogger)

struct _GSA2SOELoggerClass {
    SOELoggerClass super;
};

struct _GSA2SOELogger {
    SOELogger super;
};

GSA2SOELogger *gsa2_soe_logger_new(void);

G_END_DECLS

#endif //LIBRARY_GS_AUTH_2_GSA2SOELOGGER_H
