//
// Created by root on 05.06.2020.
//

#ifndef LIBRARY_GS_AUTH_2_GSA2JSEPARSER_H
#define LIBRARY_GS_AUTH_2_GSA2JSEPARSER_H

#include "gsa2main.h"

G_BEGIN_DECLS

#define GSA2_TYPE_JSE_PARSER gsa2_jse_parser_get_type()

GE_DECLARE_DERIVABLE_TYPE(GSA2JSEParser, gsa2_jse_parser, GSA2, JSE_PARSER, JSEParser)

struct _GSA2JSEParserClass {
    JSEParserClass super;
};

struct _GSA2JSEParser {
    JSEParser super;
};

GSA2JSEParser *gsa2_jse_parser_new(void);

G_END_DECLS

#endif //LIBRARY_GS_AUTH_2_GSA2JSEPARSER_H
