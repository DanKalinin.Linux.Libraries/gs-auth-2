//
// Created by root on 05.06.2020.
//

#ifndef LIBRARY_GS_AUTH_2_GSA2JSEREADER_H
#define LIBRARY_GS_AUTH_2_GSA2JSEREADER_H

#include "gsa2main.h"
#include "gsa2schema.h"
#include "gsa2sqleconnection.h"

G_BEGIN_DECLS

gboolean gsa2_jse_reader_identity_check(JsonObject *self);
GDateTime *gsa2_jse_reader_code_send(JsonObject *self);
GSA2Account *gsa2_jse_reader_account_create(JsonObject *self, GSA2SQLEConnection *connection, GDateTime **expiration, GError **error);
GSA2Account *gsa2_jse_reader_account_get(JsonObject *self, GSA2SQLEConnection *connection, GError **error);
void gsa2_jse_reader_password_update(JsonObject *self);
GSA2DomainFull *gsa2_jse_reader_domain_create(JsonObject *self, GSA2SQLEConnection *connection, gchar *account_id, GError **error);
gboolean gsa2_jse_reader_domain_rename(JsonObject *self, GSA2SQLEConnection *connection, gchar *domain_id, GError **error);
gboolean gsa2_jse_reader_domain_delete(JsonObject *self, GSA2SQLEConnection *connection, gchar *account_id, gchar *domain_id, GError **error);
gboolean gsa2_jse_reader_domain_exit(JsonObject *self, GSA2SQLEConnection *connection, gchar *account_id, gchar *domain_id, GError **error);
GSA2DomainFull *gsa2_jse_reader_guest_delete(JsonObject *self, GSA2SQLEConnection *connection, gchar *account_id, GError **error);
GSA2Invitation *gsa2_jse_reader_invitation_create(JsonObject *self, GSA2SQLEConnection *connection, gchar *domain_id, GError **error);
gboolean gsa2_jse_reader_invitation_revoke(JsonObject *self, GSA2SQLEConnection *connection, gchar *domain_id, gchar *invitation_id, GError **error);
GSA2DomainFull *gsa2_jse_reader_invitation_accept(JsonObject *self, GSA2SQLEConnection *connection, gchar *account_id, GError **error);
gchar *gsa2_jse_reader_code_get(JsonObject *self);
gboolean gsa2_jse_reader_controllers_add(JsonObject *self, GSA2SQLEConnection *connection, gchar *domain_id, GList **controllers_full, GError **error);
gboolean gsa2_jse_reader_controller_remove(JsonObject *self, GSA2SQLEConnection *connection, gchar *domain_id, gchar *controller_id, GError **error);
GSA2Controller *gsa2_jse_reader_controller_create(JsonObject *self, GSA2SQLEConnection *connection, GError **error);
gboolean gsa2_jse_reader_controller_verify(JsonObject *self, GSA2SQLEConnection *connection, gchar *controller_id, GError **error);
GSA2Token *gsa2_jse_reader_token_issue(JsonObject *self, GSA2SQLEConnection *connection, GError **error);
GSA2Token *gsa2_jse_reader_token_refresh(JsonObject *self, GSA2SQLEConnection *connection, GError **error);
gboolean gsa2_jse_reader_token_revoke(JsonObject *self, GSA2SQLEConnection *connection, GError **error);
gchar *gsa2_jse_reader_camera_playlist(JsonObject *self);

GSA2Account *gsa2_jse_reader_account(JsonObject *self, GSA2SQLEConnection *connection, GError **error);
GSA2AccountFull *gsa2_jse_reader_owner_full(JsonObject *self, GSA2SQLEConnection *connection, gchar *account_id, gchar *domain_id, GError **error);
GSA2AccountFull *gsa2_jse_reader_guest_full(JsonObject *self, GSA2SQLEConnection *connection, gchar *account_id, gchar *domain_id, GError **error);
gboolean gsa2_jse_reader_guests_full(JsonArray *self, GSA2SQLEConnection *connection, gchar *account_id, gchar *domain_id, GList **guests_full, GError **error);

GSA2DomainFull *gsa2_jse_reader_domain_full(JsonObject *self, GSA2SQLEConnection *connection, gchar *account_id, GError **error);
gboolean gsa2_jse_reader_domains_full(JsonArray *self, GSA2SQLEConnection *connection, gchar *account_id, GList **domains_full, GError **error);

GSA2Controller *gsa2_jse_reader_controller(JsonObject *self, GSA2SQLEConnection *connection, GError **error);
GSA2ControllerFull *gsa2_jse_reader_controller_full(JsonObject *self, GSA2SQLEConnection *connection, gchar *domain_id, GError **error);
gboolean gsa2_jse_reader_controllers_full(JsonArray *self, GSA2SQLEConnection *connection, gchar *domain_id, GList **controllers_full, GError **error);
gboolean gsa2_jse_reader_controllers_full_v1(JsonArray *self, GSA2SQLEConnection *connection, gchar *domain_id, GList **controllers_full, GError **error);

GSA2Identity *gsa2_jse_reader_identity(JsonObject *self, GSA2SQLEConnection *connection, gchar *account_id, GError **error);
gboolean gsa2_jse_reader_identities(JsonArray *self, GSA2SQLEConnection *connection, gchar *account_id, GList **identities, GError **error);

GSA2Invitation *gsa2_jse_reader_invitation(JsonObject *self, GSA2SQLEConnection *connection, gchar *domain_id, GError **error);
gboolean gsa2_jse_reader_invitations(JsonArray *self, GSA2SQLEConnection *connection, gchar *domain_id, GList **invitations, GError **error);

GSA2Token *gsa2_jse_reader_token(JsonObject *self, GSA2SQLEConnection *connection, GError **error);

GDateTime *gsa2_jse_reader_expiration(JsonObject *self);

GError *gsa2_jse_reader_error(JsonObject *self);

G_END_DECLS

#endif //LIBRARY_GS_AUTH_2_GSA2JSEREADER_H
