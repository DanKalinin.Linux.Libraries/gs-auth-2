//
// Created by root on 05.06.2020.
//

#include "gsa2jsegenerator.h"

G_DEFINE_TYPE(GSA2JSEGenerator, gsa2_jse_generator, JSE_TYPE_GENERATOR)

static void gsa2_jse_generator_class_init(GSA2JSEGeneratorClass *class) {

}

static void gsa2_jse_generator_init(GSA2JSEGenerator *self) {

}

GSA2JSEGenerator *gsa2_jse_generator_new(JsonNode *root) {
    GSA2JSEGenerator *self = g_object_new(GSA2_TYPE_JSE_GENERATOR, "root", root, NULL);
    return self;
}
