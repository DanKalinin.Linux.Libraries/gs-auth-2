//
// Created by root on 05.06.2020.
//

#ifndef LIBRARY_GS_AUTH_2_GSA2JSEGENERATOR_H
#define LIBRARY_GS_AUTH_2_GSA2JSEGENERATOR_H

#include "gsa2main.h"

G_BEGIN_DECLS

#define GSA2_TYPE_JSE_GENERATOR gsa2_jse_generator_get_type()

GE_DECLARE_DERIVABLE_TYPE(GSA2JSEGenerator, gsa2_jse_generator, GSA2, JSE_GENERATOR, JSEGenerator)

struct _GSA2JSEGeneratorClass {
    JSEGeneratorClass super;
};

struct _GSA2JSEGenerator {
    JSEGenerator super;
};

GSA2JSEGenerator *gsa2_jse_generator_new(JsonNode *root);

G_END_DECLS

#endif //LIBRARY_GS_AUTH_2_GSA2JSEGENERATOR_H
