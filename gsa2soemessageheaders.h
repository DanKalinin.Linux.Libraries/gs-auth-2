//
// Created by root on 17.11.2020.
//

#ifndef LIBRARY_GS_AUTH_2_GSHASOEMESSAGEHEADERS_H
#define LIBRARY_GS_AUTH_2_GSHASOEMESSAGEHEADERS_H

#include "gsa2main.h"

G_BEGIN_DECLS

void gsa2_soe_message_headers_set_x_controller_id(SoupMessageHeaders *self, gchar *id);
void gsa2_soe_message_headers_set_x_legacy_formatting(SoupMessageHeaders *self, gboolean legacy);

G_END_DECLS

#endif //LIBRARY_GS_AUTH_2_GSHASOEMESSAGEHEADERS_H
