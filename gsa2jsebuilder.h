//
// Created by root on 05.06.2020.
//

#ifndef LIBRARY_GS_AUTH_2_GSA2JSEBUILDER_H
#define LIBRARY_GS_AUTH_2_GSA2JSEBUILDER_H

#include "gsa2main.h"
#include "gsa2schema.h"

G_BEGIN_DECLS

JsonObject *gsa2_jse_builder_identity_check(GSA2Identity *identity);
JsonObject *gsa2_jse_builder_code_send(GSA2Identity *identity);
JsonObject *gsa2_jse_builder_account_create(GSA2Account *account, GList *identities);
JsonObject *gsa2_jse_builder_password_update(gchar *password);
JsonObject *gsa2_jse_builder_domain_create(GSA2Domain *domain, GList *controllers_full);
JsonObject *gsa2_jse_builder_domain_rename(gchar *name);
JsonObject *gsa2_jse_builder_invitation_create(GSA2Invitation *invitation);
JsonObject *gsa2_jse_builder_invitation_accept(gchar *invitation_id);
JsonObject *gsa2_jse_builder_controllers_add(GList *controllers_full);
JsonObject *gsa2_jse_builder_controller_create(GSA2Controller *controller);
JsonObject *gsa2_jse_builder_controller_verify(gchar *code);
JsonObject *gsa2_jse_builder_token_issue(GSA2Identity *identity, GSA2Credential *credential);
JsonObject *gsa2_jse_builder_token_refresh(gchar *refresh);
JsonObject *gsa2_jse_builder_token_revoke(gchar *access);
JsonObject *gsa2_jse_builder_camera_playlist(gchar *key);

JsonObject *gsa2_jse_builder_account(GSA2Account *account, GList *identities);

JsonObject *gsa2_jse_builder_domain(GSA2Domain *domain, GList *controllers_full);

JsonObject *gsa2_jse_builder_controller(GSA2Controller *controller);
JsonObject *gsa2_jse_builder_controller_full(GSA2ControllerFull *controller_full);
JsonArray *gsa2_jse_builder_controllers_full(GList *controllers_full);

JsonObject *gsa2_jse_builder_identity(GSA2Identity *identity);
JsonArray *gsa2_jse_builder_identities(GList *identities);

JsonObject *gsa2_jse_builder_invitation(GSA2Invitation *invitation);

JsonObject *gsa2_jse_builder_credential(GSA2Credential *credential);

G_END_DECLS

#endif //LIBRARY_GS_AUTH_2_GSA2JSEBUILDER_H
