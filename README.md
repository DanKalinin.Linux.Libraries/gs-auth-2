## gs-auth-2

GSLabs SmartHome authorization SDK for Linux.

This library provides a [binding interface](gsa2sdk.h) for high-level languages and can be compiled for POSIX-compatible systems, such as Android and iOS.

- [iOS SDK](https://gitlab.com/DanKalinin.iOS.Frameworks/GSAuth2.git)
- [Android SDK](https://gitlab.com/DanKalinin.Android/GSAuth2.git)

![](images/sdk.jpeg)
