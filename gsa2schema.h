//
// Created by root on 27.05.2020.
//

#ifndef LIBRARY_GS_AUTH_2_GSA2SCHEMA_H
#define LIBRARY_GS_AUTH_2_GSA2SCHEMA_H

#include "gsa2main.h"










G_BEGIN_DECLS

typedef struct {
    gchar *id;
    gchar *name;
    gchar *password;
} GSA2Account;

GE_STRUCTURE_FIELD(gsa2_account, id, GSA2Account, gchar, g_strdup, g_free, NULL)
GE_STRUCTURE_FIELD(gsa2_account, name, GSA2Account, gchar, g_strdup, g_free, NULL)
GE_STRUCTURE_FIELD(gsa2_account, password, GSA2Account, gchar, g_strdup, g_free, NULL)

GSA2Account *gsa2_account_copy(GSA2Account *self);
void gsa2_account_free(GSA2Account *self);

G_DEFINE_AUTOPTR_CLEANUP_FUNC(GSA2Account, gsa2_account_free)

G_END_DECLS










G_BEGIN_DECLS

typedef struct {
    gchar *id;
    gchar *owner;
    gchar *name;
} GSA2Domain;

GE_STRUCTURE_FIELD(gsa2_domain, id, GSA2Domain, gchar, g_strdup, g_free, NULL)
GE_STRUCTURE_FIELD(gsa2_domain, owner, GSA2Domain, gchar, g_strdup, g_free, NULL)
GE_STRUCTURE_FIELD(gsa2_domain, name, GSA2Domain, gchar, g_strdup, g_free, NULL)

GSA2Domain *gsa2_domain_copy(GSA2Domain *self);
void gsa2_domain_free(GSA2Domain *self);

G_DEFINE_AUTOPTR_CLEANUP_FUNC(GSA2Domain, gsa2_domain_free)

G_END_DECLS










G_BEGIN_DECLS

typedef struct {
    gchar *id;
    gchar *mac;
    gchar *model;
    gchar *serial;
} GSA2Controller;

GE_STRUCTURE_FIELD(gsa2_controller, id, GSA2Controller, gchar, g_strdup, g_free, NULL)
GE_STRUCTURE_FIELD(gsa2_controller, mac, GSA2Controller, gchar, g_strdup, g_free, NULL)
GE_STRUCTURE_FIELD(gsa2_controller, model, GSA2Controller, gchar, g_strdup, g_free, NULL)
GE_STRUCTURE_FIELD(gsa2_controller, serial, GSA2Controller, gchar, g_strdup, g_free, NULL)

GSA2Controller *gsa2_controller_copy(GSA2Controller *self);
void gsa2_controller_free(GSA2Controller *self);

G_DEFINE_AUTOPTR_CLEANUP_FUNC(GSA2Controller, gsa2_controller_free)

G_END_DECLS










G_BEGIN_DECLS

typedef struct {
    gchar *account;
    gchar *domain;
    gchar *expiration;
    gint current;
} GSA2AccountDomain;

GE_STRUCTURE_FIELD(gsa2_account_domain, account, GSA2AccountDomain, gchar, g_strdup, g_free, NULL)
GE_STRUCTURE_FIELD(gsa2_account_domain, domain, GSA2AccountDomain, gchar, g_strdup, g_free, NULL)
GE_STRUCTURE_FIELD(gsa2_account_domain, expiration, GSA2AccountDomain, gchar, g_strdup, g_free, NULL)

GSA2AccountDomain *gsa2_account_domain_copy(GSA2AccountDomain *self);
void gsa2_account_domain_free(GSA2AccountDomain *self);

G_DEFINE_AUTOPTR_CLEANUP_FUNC(GSA2AccountDomain, gsa2_account_domain_free)

G_END_DECLS










G_BEGIN_DECLS

typedef struct {
    gchar *domain;
    gchar *controller;
    gchar *key;
    gchar *ip;
    gint port;
    gchar *bssid;
    gint verified;
} GSA2DomainController;

GE_STRUCTURE_FIELD(gsa2_domain_controller, domain, GSA2DomainController, gchar, g_strdup, g_free, NULL)
GE_STRUCTURE_FIELD(gsa2_domain_controller, controller, GSA2DomainController, gchar, g_strdup, g_free, NULL)
GE_STRUCTURE_FIELD(gsa2_domain_controller, key, GSA2DomainController, gchar, g_strdup, g_free, NULL)
GE_STRUCTURE_FIELD(gsa2_domain_controller, ip, GSA2DomainController, gchar, g_strdup, g_free, NULL)
GE_STRUCTURE_FIELD(gsa2_domain_controller, bssid, GSA2DomainController, gchar, g_strdup, g_free, NULL)

GSA2DomainController *gsa2_domain_controller_copy(GSA2DomainController *self);
void gsa2_domain_controller_free(GSA2DomainController *self);

G_DEFINE_AUTOPTR_CLEANUP_FUNC(GSA2DomainController, gsa2_domain_controller_free)

G_END_DECLS










G_BEGIN_DECLS

#define GSA2_IDENTITY_TYPE_PHONE "phone"
#define GSA2_IDENTITY_TYPE_EMAIL "email"
#define GSA2_IDENTITY_TYPE_MAC "macAddress"

typedef struct {
    gchar *type;
    gchar *value;
    gchar *account;
    gchar *controller;
} GSA2Identity;

GE_STRUCTURE_FIELD(gsa2_identity, type, GSA2Identity, gchar, g_strdup, g_free, NULL)
GE_STRUCTURE_FIELD(gsa2_identity, value, GSA2Identity, gchar, g_strdup, g_free, NULL)
GE_STRUCTURE_FIELD(gsa2_identity, account, GSA2Identity, gchar, g_strdup, g_free, NULL)
GE_STRUCTURE_FIELD(gsa2_identity, controller, GSA2Identity, gchar, g_strdup, g_free, NULL)

GSA2Identity *gsa2_identity_copy(GSA2Identity *self);
void gsa2_identity_free(GSA2Identity *self);

G_DEFINE_AUTOPTR_CLEANUP_FUNC(GSA2Identity, gsa2_identity_free)

G_END_DECLS










G_BEGIN_DECLS

typedef struct {
    gchar *id;
    gchar *domain;
    gchar *name;
    gchar *expiration;
    gint64 access;
} GSA2Invitation;

GE_STRUCTURE_FIELD(gsa2_invitation, id, GSA2Invitation, gchar, g_strdup, g_free, NULL)
GE_STRUCTURE_FIELD(gsa2_invitation, domain, GSA2Invitation, gchar, g_strdup, g_free, NULL)
GE_STRUCTURE_FIELD(gsa2_invitation, name, GSA2Invitation, gchar, g_strdup, g_free, NULL)
GE_STRUCTURE_FIELD(gsa2_invitation, expiration, GSA2Invitation, gchar, g_strdup, g_free, NULL)

GSA2Invitation *gsa2_invitation_copy(GSA2Invitation *self);
void gsa2_invitation_free(GSA2Invitation *self);

G_DEFINE_AUTOPTR_CLEANUP_FUNC(GSA2Invitation, gsa2_invitation_free)

G_END_DECLS










G_BEGIN_DECLS

#define GSA2_TOKEN_SUBJECT_ACCOUNT "account"
#define GSA2_TOKEN_SUBJECT_CONTROLLER "controller"

typedef struct {
    gchar *subject;
    gchar *id;
    gchar *access;
    gchar *refresh;
} GSA2Token;

GE_STRUCTURE_FIELD(gsa2_token, subject, GSA2Token, gchar, g_strdup, g_free, NULL)
GE_STRUCTURE_FIELD(gsa2_token, id, GSA2Token, gchar, g_strdup, g_free, NULL)
GE_STRUCTURE_FIELD(gsa2_token, access, GSA2Token, gchar, g_strdup, g_free, NULL)
GE_STRUCTURE_FIELD(gsa2_token, refresh, GSA2Token, gchar, g_strdup, g_free, NULL)

GSA2Token *gsa2_token_copy(GSA2Token *self);
void gsa2_token_free(GSA2Token *self);

G_DEFINE_AUTOPTR_CLEANUP_FUNC(GSA2Token, gsa2_token_free)

G_END_DECLS










G_BEGIN_DECLS

#define GSA2_CREDENTIAL_TYPE_PASSWORD "password"
#define GSA2_CREDENTIAL_TYPE_CODE "activationCode"
#define GSA2_CREDENTIAL_TYPE_SERIAL "serialNo"

typedef struct {
    gchar *type;
    gchar *value;
} GSA2Credential;

GE_STRUCTURE_FIELD(gsa2_credential, type, GSA2Credential, gchar, g_strdup, g_free, NULL)
GE_STRUCTURE_FIELD(gsa2_credential, value, GSA2Credential, gchar, g_strdup, g_free, NULL)

GSA2Credential *gsa2_credential_copy(GSA2Credential *self);
void gsa2_credential_free(GSA2Credential *self);

G_DEFINE_AUTOPTR_CLEANUP_FUNC(GSA2Credential, gsa2_credential_free)

G_END_DECLS










G_BEGIN_DECLS

typedef struct {
    GSA2Account *account;
    GSA2AccountDomain *account_domain;
} GSA2AccountFull;

GE_STRUCTURE_FIELD(gsa2_account_full, account, GSA2AccountFull, GSA2Account, gsa2_account_copy, gsa2_account_free, NULL)
GE_STRUCTURE_FIELD(gsa2_account_full, account_domain, GSA2AccountFull, GSA2AccountDomain, gsa2_account_domain_copy, gsa2_account_domain_free, NULL)

GSA2AccountFull *gsa2_account_full_copy(GSA2AccountFull *self);
void gsa2_account_full_free(GSA2AccountFull *self);

G_DEFINE_AUTOPTR_CLEANUP_FUNC(GSA2AccountFull, gsa2_account_full_free)

G_END_DECLS










G_BEGIN_DECLS

typedef struct {
    GSA2Domain *domain;
    GSA2AccountDomain *account_domain;
} GSA2DomainFull;

GE_STRUCTURE_FIELD(gsa2_domain_full, domain, GSA2DomainFull, GSA2Domain, gsa2_domain_copy, gsa2_domain_free, NULL)
GE_STRUCTURE_FIELD(gsa2_domain_full, account_domain, GSA2DomainFull, GSA2AccountDomain, gsa2_account_domain_copy, gsa2_account_domain_free, NULL)

GSA2DomainFull *gsa2_domain_full_copy(GSA2DomainFull *self);
void gsa2_domain_full_free(GSA2DomainFull *self);

G_DEFINE_AUTOPTR_CLEANUP_FUNC(GSA2DomainFull, gsa2_domain_full_free)

G_END_DECLS










G_BEGIN_DECLS

typedef struct {
    GSA2Controller *controller;
    GSA2DomainController *domain_controller;
} GSA2ControllerFull;

GE_STRUCTURE_FIELD(gsa2_controller_full, controller, GSA2ControllerFull, GSA2Controller, gsa2_controller_copy, gsa2_controller_free, NULL)
GE_STRUCTURE_FIELD(gsa2_controller_full, domain_controller, GSA2ControllerFull, GSA2DomainController, gsa2_domain_controller_copy, gsa2_domain_controller_free, NULL)

GSA2ControllerFull *gsa2_controller_full_copy(GSA2ControllerFull *self);
void gsa2_controller_full_free(GSA2ControllerFull *self);

G_DEFINE_AUTOPTR_CLEANUP_FUNC(GSA2ControllerFull, gsa2_controller_full_free)

G_END_DECLS










#endif //LIBRARY_GS_AUTH_2_GSA2SCHEMA_H
