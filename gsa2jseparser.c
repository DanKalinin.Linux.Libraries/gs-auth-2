//
// Created by root on 05.06.2020.
//

#include "gsa2jseparser.h"

G_DEFINE_TYPE(GSA2JSEParser, gsa2_jse_parser, JSE_TYPE_PARSER)

static void gsa2_jse_parser_class_init(GSA2JSEParserClass *class) {

}

static void gsa2_jse_parser_init(GSA2JSEParser *self) {

}

GSA2JSEParser *gsa2_jse_parser_new(void) {
    GSA2JSEParser *self = g_object_new(GSA2_TYPE_JSE_PARSER, NULL);
    return self;
}
