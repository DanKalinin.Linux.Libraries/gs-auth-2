//
// Created by root on 27.05.2020.
//

#include "gsa2sqleconnection.h"

enum {
    GSA2_SQLE_CONNECTION_SIGNAL_ACCOUNT_DELETING = 1,
    GSA2_SQLE_CONNECTION_SIGNAL_ACCOUNT_DOMAIN_DELETING,
    GSA2_SQLE_CONNECTION_SIGNAL_DOMAIN_CONTROLLER_DELETING,
    _GSA2_SQLE_CONNECTION_SIGNAL_COUNT
};

guint gsa2_sqle_connection_signals[_GSA2_SQLE_CONNECTION_SIGNAL_COUNT] = {0};

void gsa2_sqle_connection_preupdate(SQLEConnection *self, sqlite3 *database, gint operation, gchar *schema, gchar *table, gint64 rowid, gint64 new_rowid);
void _gsa2_sqle_connection_account_deleting(GSA2SQLEConnection *self, GSA2Account *account);
void _gsa2_sqle_connection_account_domain_deleting(GSA2SQLEConnection *self, GSA2AccountDomain *account_domain);
void _gsa2_sqle_connection_domain_controller_deleting(GSA2SQLEConnection *self, GSA2DomainController *domain_controller);

G_DEFINE_TYPE(GSA2SQLEConnection, gsa2_sqle_connection, SQLE_TYPE_CONNECTION)

static void gsa2_sqle_connection_class_init(GSA2SQLEConnectionClass *class) {
    SQLE_CONNECTION_CLASS(class)->preupdate = gsa2_sqle_connection_preupdate;

    class->account_deleting = _gsa2_sqle_connection_account_deleting;
    class->account_domain_deleting = _gsa2_sqle_connection_account_domain_deleting;
    class->domain_controller_deleting = _gsa2_sqle_connection_domain_controller_deleting;

    gsa2_sqle_connection_signals[GSA2_SQLE_CONNECTION_SIGNAL_ACCOUNT_DELETING] = g_signal_new("account-deleting", GSA2_TYPE_SQLE_CONNECTION, G_SIGNAL_RUN_LAST, G_STRUCT_OFFSET(GSA2SQLEConnectionClass, account_deleting), NULL, NULL, NULL, G_TYPE_NONE, 1, G_TYPE_POINTER);
    gsa2_sqle_connection_signals[GSA2_SQLE_CONNECTION_SIGNAL_ACCOUNT_DOMAIN_DELETING] = g_signal_new("account-domain-deleting", GSA2_TYPE_SQLE_CONNECTION, G_SIGNAL_RUN_LAST, G_STRUCT_OFFSET(GSA2SQLEConnectionClass, account_domain_deleting), NULL, NULL, NULL, G_TYPE_NONE, 1, G_TYPE_POINTER);
    gsa2_sqle_connection_signals[GSA2_SQLE_CONNECTION_SIGNAL_DOMAIN_CONTROLLER_DELETING] = g_signal_new("domain-controller-deleting", GSA2_TYPE_SQLE_CONNECTION, G_SIGNAL_RUN_LAST, G_STRUCT_OFFSET(GSA2SQLEConnectionClass, domain_controller_deleting), NULL, NULL, NULL, G_TYPE_NONE, 1, G_TYPE_POINTER);
}

static void gsa2_sqle_connection_init(GSA2SQLEConnection *self) {

}

void gsa2_sqle_connection_preupdate(SQLEConnection *self, sqlite3 *database, gint operation, gchar *schema, gchar *table, gint64 rowid, gint64 new_rowid) {
    if (g_strcmp0(table, "account") == 0) {
        if (operation == SQLITE_DELETE) {
            g_autoptr(GSA2Account) account = NULL;
            if (gsa2_sqle_connection_account_preupdate(GSA2_SQLE_CONNECTION(self), FALSE, &account, NULL) != SQLITE_OK) return;
            gsa2_sqle_connection_account_deleting(GSA2_SQLE_CONNECTION(self), account);
        }
    } else if (g_strcmp0(table, "account_domain") == 0) {
        if (operation == SQLITE_DELETE) {
            g_autoptr(GSA2AccountDomain) account_domain = NULL;
            if (gsa2_sqle_connection_account_domain_preupdate(GSA2_SQLE_CONNECTION(self), FALSE, &account_domain, NULL) != SQLITE_OK) return;
            gsa2_sqle_connection_account_domain_deleting(GSA2_SQLE_CONNECTION(self), account_domain);
        }
    } else if (g_strcmp0(table, "domain_controller") == 0) {
        if (operation == SQLITE_DELETE) {
            g_autoptr(GSA2DomainController) domain_controller = NULL;
            if (gsa2_sqle_connection_domain_controller_preupdate(GSA2_SQLE_CONNECTION(self), FALSE, &domain_controller, NULL) != SQLITE_OK) return;
            gsa2_sqle_connection_domain_controller_deleting(GSA2_SQLE_CONNECTION(self), domain_controller);
        }
    }
}

void gsa2_sqle_connection_account_deleting(GSA2SQLEConnection *self, GSA2Account *account) {
    g_signal_emit(self, gsa2_sqle_connection_signals[GSA2_SQLE_CONNECTION_SIGNAL_ACCOUNT_DELETING], 0, account);
}

void _gsa2_sqle_connection_account_deleting(GSA2SQLEConnection *self, GSA2Account *account) {
    g_print("account-deleting\n");
}

void gsa2_sqle_connection_account_domain_deleting(GSA2SQLEConnection *self, GSA2AccountDomain *account_domain) {
    g_signal_emit(self, gsa2_sqle_connection_signals[GSA2_SQLE_CONNECTION_SIGNAL_ACCOUNT_DOMAIN_DELETING], 0, account_domain);
}

void _gsa2_sqle_connection_account_domain_deleting(GSA2SQLEConnection *self, GSA2AccountDomain *account_domain) {
    g_print("account-domain-deleting\n");
}

void gsa2_sqle_connection_domain_controller_deleting(GSA2SQLEConnection *self, GSA2DomainController *domain_controller) {
    g_signal_emit(self, gsa2_sqle_connection_signals[GSA2_SQLE_CONNECTION_SIGNAL_DOMAIN_CONTROLLER_DELETING], 0, domain_controller);
}

void _gsa2_sqle_connection_domain_controller_deleting(GSA2SQLEConnection *self, GSA2DomainController *domain_controller) {
    g_print("domain-controller-deleting\n");
}

GSA2SQLEConnection *gsa2_sqle_connection_new(gchar *file, GError **error) {
    if (!gsa2_sqle_database_set(file, error)) return NULL;

    g_autoptr(sqlite3) object = NULL;
    if (sqle_connection_open(file, &object, error) != SQLITE_OK) return NULL;
    if (sqle_connection_exec(object, "PRAGMA foreign_keys = ON", NULL, NULL, NULL, error) != SQLITE_OK) return NULL;
    if (sqle_connection_busy_timeout(object, 1000, error) != SQLITE_OK) return NULL;
    if (sqle_carray_init(object, NULL, NULL, error) != SQLITE_OK) return NULL;

    GSA2SQLEConnection *self = g_object_new(GSA2_TYPE_SQLE_CONNECTION, NULL);
    (void)sqlite3_preupdate_hook(object, sqle_connection_preupdate_callback, self);
    SQLE_CONNECTION(self)->object = g_steal_pointer(&object);
    return self;
}

gint gsa2_sqle_connection_account_upsert(GSA2SQLEConnection *self, GSA2Account *account, GError **error) {
    gint ret = SQLITE_OK;
    g_autoptr(sqlite3_stmt) statement = NULL;
    if ((ret = sqle_connection_prepare_v2(SQLE_CONNECTION(self)->object, "INSERT INTO account (id, name, password) VALUES (?1, ?2, ?3) ON CONFLICT (id) DO UPDATE SET name = ?2, password = ?3", -1, &statement, NULL, error)) != SQLITE_OK) return ret;
    if ((ret = gsa2_sqle_statement_bind_account(statement, account, error)) != SQLITE_OK) return ret;
    if ((ret = sqle_statement_step(statement, error)) != SQLITE_DONE) return ret;
    return ret;
}

gint gsa2_sqle_connection_account_delete(GSA2SQLEConnection *self, gchar *tail, GError **error) {
    gint ret = SQLITE_OK;
    g_autofree gchar *sql = g_strjoin(" ", "DELETE FROM account", tail, NULL);
    g_autoptr(sqlite3_stmt) statement = NULL;
    if ((ret = sqle_connection_prepare_v2(SQLE_CONNECTION(self)->object, sql, -1, &statement, NULL, error)) != SQLITE_OK) return ret;
    if ((ret = sqle_statement_step(statement, error)) != SQLITE_DONE) return ret;
    return ret;
}

gint gsa2_sqle_connection_account_select(GSA2SQLEConnection *self, gchar *tail, GList **accounts, GError **error) {
    gint ret = SQLITE_OK;
    g_autofree gchar *sql = g_strjoin(" ", "SELECT * FROM account", tail, NULL);
    g_autoptr(sqlite3_stmt) statement = NULL;
    if ((ret = sqle_connection_prepare_v2(SQLE_CONNECTION(self)->object, sql, -1, &statement, NULL, error)) != SQLITE_OK) return ret;
    if ((ret = sqle_statement_execute(statement, accounts, gsa2_sqle_statement_account, NULL, error)) != SQLITE_DONE) return ret;
    return ret;
}

gint gsa2_sqle_connection_account_preupdate(GSA2SQLEConnection *self, gboolean new, GSA2Account **account, GError **error) {
    gint ret = SQLITE_OK;

    sqlite3_value *value_id = NULL;
    if ((ret = sqle_connection_preupdate_value(SQLE_CONNECTION(self)->object, new, 0, &value_id, error)) != SQLITE_OK) return ret;

    sqlite3_value *value_name = NULL;
    if ((ret = sqle_connection_preupdate_value(SQLE_CONNECTION(self)->object, new, 1, &value_name, error)) != SQLITE_OK) return ret;

    sqlite3_value *value_password = NULL;
    if ((ret = sqle_connection_preupdate_value(SQLE_CONNECTION(self)->object, new, 2, &value_password, error)) != SQLITE_OK) return ret;

    GSA2Account ret_account = {0};
    ret_account.id = (gchar *)sqlite3_value_text(value_id);
    ret_account.name = (gchar *)sqlite3_value_text(value_name);
    ret_account.password = (gchar *)sqlite3_value_text(value_password);

    GE_SET_VALUE(account, gsa2_account_copy(&ret_account));
    return ret;
}

gint gsa2_sqle_connection_domain_upsert(GSA2SQLEConnection *self, GSA2Domain *domain, GError **error) {
    gint ret = SQLITE_OK;
    g_autoptr(sqlite3_stmt) statement = NULL;
    if ((ret = sqle_connection_prepare_v2(SQLE_CONNECTION(self)->object, "INSERT INTO domain (id, owner, name) VALUES (?1, ?2, ?3) ON CONFLICT (id) DO UPDATE SET owner = ?2, name = ?3", -1, &statement, NULL, error)) != SQLITE_OK) return ret;
    if ((ret = gsa2_sqle_statement_bind_domain(statement, domain, error)) != SQLITE_OK) return ret;
    if ((ret = sqle_statement_step(statement, error)) != SQLITE_DONE) return ret;
    return ret;
}

gint gsa2_sqle_connection_domain_update_name_by_id(GSA2SQLEConnection *self, gchar *id, gchar *name, GError **error) {
    gint ret = SQLITE_OK;
    g_autoptr(sqlite3_stmt) statement = NULL;
    if ((ret = sqle_connection_prepare_v2(SQLE_CONNECTION(self)->object, "UPDATE domain SET name = ?1 WHERE id = ?2", -1, &statement, NULL, error)) != SQLITE_OK) return ret;
    if ((ret = sqle_statement_bind_text(statement, 1, name, -1, SQLITE_TRANSIENT, error)) != SQLITE_OK) return ret;
    if ((ret = sqle_statement_bind_text(statement, 2, id, -1, SQLITE_TRANSIENT, error)) != SQLITE_OK) return ret;
    if ((ret = sqle_statement_step(statement, error)) != SQLITE_DONE) return ret;
    return ret;
}

gint gsa2_sqle_connection_domain_select(GSA2SQLEConnection *self, gchar *tail, GList **domains, GError **error) {
    gint ret = SQLITE_OK;
    g_autofree gchar *sql = g_strjoin(" ", "SELECT * FROM domain", tail, NULL);
    g_autoptr(sqlite3_stmt) statement = NULL;
    if ((ret = sqle_connection_prepare_v2(SQLE_CONNECTION(self)->object, sql, -1, &statement, NULL, error)) != SQLITE_OK) return ret;
    if ((ret = sqle_statement_execute(statement, domains, gsa2_sqle_statement_domain, NULL, error)) != SQLITE_DONE) return ret;
    return ret;
}

gint gsa2_sqle_connection_controller_upsert(GSA2SQLEConnection *self, GSA2Controller *controller, GError **error) {
    gint ret = SQLITE_OK;
    g_autoptr(sqlite3_stmt) statement = NULL;
    if ((ret = sqle_connection_prepare_v2(SQLE_CONNECTION(self)->object, "INSERT INTO controller (id, mac, model, serial) VALUES (?1, ?2, ?3, ?4) ON CONFLICT (id) DO UPDATE SET mac = ?2, model = ?3, serial = ?4", -1, &statement, NULL, error)) != SQLITE_OK) return ret;
    if ((ret = gsa2_sqle_statement_bind_controller(statement, controller, error)) != SQLITE_OK) return ret;
    if ((ret = sqle_statement_step(statement, error)) != SQLITE_DONE) return ret;
    return ret;
}

gint gsa2_sqle_connection_controller_select(GSA2SQLEConnection *self, gchar *tail, GList **controllers, GError **error) {
    gint ret = SQLITE_OK;
    g_autofree gchar *sql = g_strjoin(" ", "SELECT * FROM controller", tail, NULL);
    g_autoptr(sqlite3_stmt) statement = NULL;
    if ((ret = sqle_connection_prepare_v2(SQLE_CONNECTION(self)->object, sql, -1, &statement, NULL, error)) != SQLITE_OK) return ret;
    if ((ret = sqle_statement_execute(statement, controllers, gsa2_sqle_statement_controller, NULL, error)) != SQLITE_DONE) return ret;
    return ret;
}

gint gsa2_sqle_connection_controller_select_by_mac(GSA2SQLEConnection *self, gchar *mac, GList **controllers, GError **error) {
    gint ret = SQLITE_OK;
    g_autoptr(sqlite3_stmt) statement = NULL;
    if ((ret = sqle_connection_prepare_v2(SQLE_CONNECTION(self)->object, "SELECT * FROM controller WHERE mac = ?1", -1, &statement, NULL, error)) != SQLITE_OK) return ret;
    if ((ret = sqle_statement_bind_text(statement, 1, mac, -1, SQLITE_TRANSIENT, error)) != SQLITE_OK) return ret;
    if ((ret = sqle_statement_execute(statement, controllers, gsa2_sqle_statement_controller, NULL, error)) != SQLITE_DONE) return ret;
    return ret;
}

gint gsa2_sqle_connection_account_domain_upsert(GSA2SQLEConnection *self, GSA2AccountDomain *account_domain, GError **error) {
    gint ret = SQLITE_OK;
    g_autoptr(sqlite3_stmt) statement = NULL;
    if ((ret = sqle_connection_prepare_v2(SQLE_CONNECTION(self)->object, "INSERT INTO account_domain (account, domain, expiration, current) VALUES (?1, ?2, ?3, ?4) ON CONFLICT (account, domain) DO UPDATE SET expiration = ?3, current = ?4", -1, &statement, NULL, error)) != SQLITE_OK) return ret;
    if ((ret = gsa2_sqle_statement_bind_account_domain(statement, account_domain, error)) != SQLITE_OK) return ret;
    if ((ret = sqle_statement_step(statement, error)) != SQLITE_DONE) return ret;
    return ret;
}

gint gsa2_sqle_connection_account_domain_delete_by_account_and_domain(GSA2SQLEConnection *self, gchar *account, gchar *domain, GError **error) {
    gint ret = SQLITE_OK;
    g_autoptr(sqlite3_stmt) statement = NULL;
    if ((ret = sqle_connection_prepare_v2(SQLE_CONNECTION(self)->object, "DELETE FROM account_domain WHERE account = ?1 AND domain = ?2", -1, &statement, NULL, error)) != SQLITE_OK) return ret;
    if ((ret = sqle_statement_bind_text(statement, 1, account, -1, SQLITE_TRANSIENT, error)) != SQLITE_OK) return ret;
    if ((ret = sqle_statement_bind_text(statement, 2, domain, -1, SQLITE_TRANSIENT, error)) != SQLITE_OK) return ret;
    if ((ret = sqle_statement_step(statement, error)) != SQLITE_DONE) return ret;
    return ret;
}

gint gsa2_sqle_connection_account_domain_delete_by_account_and_domains(GSA2SQLEConnection *self, gchar *account, GPtrArray *domains, GError **error) {
    gint ret = SQLITE_OK;
    g_autoptr(sqlite3_stmt) statement = NULL;
    if ((ret = sqle_connection_prepare_v2(SQLE_CONNECTION(self)->object, "DELETE FROM account_domain WHERE account = ?1 AND domain NOT IN carray(?2, ?3, 'char*')", -1, &statement, NULL, error)) != SQLITE_OK) return ret;
    if ((ret = sqle_statement_bind_text(statement, 1, account, -1, SQLITE_TRANSIENT, error)) != SQLITE_OK) return ret;
    if ((ret = sqle_carray_bind_pointer(statement, 2, domains->pdata, SQLITE_STATIC, error)) != SQLITE_OK) return ret;
    if ((ret = sqle_statement_bind_int(statement, 3, domains->len, error)) != SQLITE_OK) return ret;
    if ((ret = sqle_statement_step(statement, error)) != SQLITE_DONE) return ret;
    return ret;
}

gint gsa2_sqle_connection_account_domain_delete_by_domain_and_accounts(GSA2SQLEConnection *self, gchar *domain, GPtrArray *accounts, GError **error) {
    gint ret = SQLITE_OK;
    g_autoptr(sqlite3_stmt) statement = NULL;
    if ((ret = sqle_connection_prepare_v2(SQLE_CONNECTION(self)->object, "DELETE FROM account_domain WHERE expiration IS NOT NULL AND domain = ?1 AND account NOT IN carray(?2, ?3, 'char*')", -1, &statement, NULL, error)) != SQLITE_OK) return ret;
    if ((ret = sqle_statement_bind_text(statement, 1, domain, -1, SQLITE_TRANSIENT, error)) != SQLITE_OK) return ret;
    if ((ret = sqle_carray_bind_pointer(statement, 2, accounts->pdata, SQLITE_STATIC, error)) != SQLITE_OK) return ret;
    if ((ret = sqle_statement_bind_int(statement, 3, accounts->len, error)) != SQLITE_OK) return ret;
    if ((ret = sqle_statement_step(statement, error)) != SQLITE_DONE) return ret;
    return ret;
}

gint gsa2_sqle_connection_account_domain_select(GSA2SQLEConnection *self, gchar *tail, GList **account_domains, GError **error) {
    gint ret = SQLITE_OK;
    g_autofree gchar *sql = g_strjoin(" ", "SELECT * FROM account_domain", tail, NULL);
    g_autoptr(sqlite3_stmt) statement = NULL;
    if ((ret = sqle_connection_prepare_v2(SQLE_CONNECTION(self)->object, sql, -1, &statement, NULL, error)) != SQLITE_OK) return ret;
    if ((ret = sqle_statement_execute(statement, account_domains, gsa2_sqle_statement_account_domain, NULL, error)) != SQLITE_DONE) return ret;
    return ret;
}

gint gsa2_sqle_connection_account_domain_preupdate(GSA2SQLEConnection *self, gboolean new, GSA2AccountDomain **account_domain, GError **error) {
    gint ret = SQLITE_OK;

    sqlite3_value *value_account = NULL;
    if ((ret = sqle_connection_preupdate_value(SQLE_CONNECTION(self)->object, new, 0, &value_account, error)) != SQLITE_OK) return ret;

    sqlite3_value *value_domain = NULL;
    if ((ret = sqle_connection_preupdate_value(SQLE_CONNECTION(self)->object, new, 1, &value_domain, error)) != SQLITE_OK) return ret;

    sqlite3_value *value_expiration = NULL;
    if ((ret = sqle_connection_preupdate_value(SQLE_CONNECTION(self)->object, new, 2, &value_expiration, error)) != SQLITE_OK) return ret;

    sqlite3_value *value_current = NULL;
    if ((ret = sqle_connection_preupdate_value(SQLE_CONNECTION(self)->object, new, 3, &value_current, error)) != SQLITE_OK) return ret;

    GSA2AccountDomain ret_account_domain = {0};
    ret_account_domain.account = (gchar *)sqlite3_value_text(value_account);
    ret_account_domain.domain = (gchar *)sqlite3_value_text(value_domain);
    ret_account_domain.expiration = (gchar *)sqlite3_value_text(value_expiration);
    ret_account_domain.current = sqlite3_value_int(value_current);

    GE_SET_VALUE(account_domain, gsa2_account_domain_copy(&ret_account_domain));
    return ret;
}

gint gsa2_sqle_connection_domain_controller_upsert(GSA2SQLEConnection *self, GSA2DomainController *domain_controller, GError **error) {
    gint ret = SQLITE_OK;
    g_autoptr(sqlite3_stmt) statement = NULL;
    if ((ret = sqle_connection_prepare_v2(SQLE_CONNECTION(self)->object, "INSERT INTO domain_controller (domain, controller, key, ip, port, bssid, verified) VALUES (?1, ?2, ?3, ?4, ?5, ?6, ?7) ON CONFLICT (domain, controller) DO UPDATE SET key = ?3, ip = ?4, port = ?5, bssid = ?6, verified = ?7", -1, &statement, NULL, error)) != SQLITE_OK) return ret;
    if ((ret = gsa2_sqle_statement_bind_domain_controller(statement, domain_controller, error)) != SQLITE_OK) return ret;
    if ((ret = sqle_statement_step(statement, error)) != SQLITE_DONE) return ret;
    return ret;
}

gint gsa2_sqle_connection_domain_controller_update_verified_by_controller(GSA2SQLEConnection *self, gchar *controller, gint verified, GError **error) {
    gint ret = SQLITE_OK;
    g_autoptr(sqlite3_stmt) statement = NULL;
    if ((ret = sqle_connection_prepare_v2(SQLE_CONNECTION(self)->object, "UPDATE domain_controller SET verified = ?1 WHERE controller = ?2", -1, &statement, NULL, error)) != SQLITE_OK) return ret;
    if ((ret = sqle_statement_bind_int(statement, 1, verified, error)) != SQLITE_OK) return ret;
    if ((ret = sqle_statement_bind_text(statement, 2, controller, -1, SQLITE_TRANSIENT, error)) != SQLITE_OK) return ret;
    if ((ret = sqle_statement_step(statement, error)) != SQLITE_DONE) return ret;
    return ret;
}

gint gsa2_sqle_connection_domain_controller_delete_by_domain_and_controller(GSA2SQLEConnection *self, gchar *domain, gchar *controller, GError **error) {
    gint ret = SQLITE_OK;
    g_autoptr(sqlite3_stmt) statement = NULL;
    if ((ret = sqle_connection_prepare_v2(SQLE_CONNECTION(self)->object, "DELETE FROM domain_controller WHERE domain = ?1 AND controller = ?2", -1, &statement, NULL, error)) != SQLITE_OK) return ret;
    if ((ret = sqle_statement_bind_text(statement, 1, domain, -1, SQLITE_TRANSIENT, error)) != SQLITE_OK) return ret;
    if ((ret = sqle_statement_bind_text(statement, 2, controller, -1, SQLITE_TRANSIENT, error)) != SQLITE_OK) return ret;
    if ((ret = sqle_statement_step(statement, error)) != SQLITE_DONE) return ret;
    return ret;
}

gint gsa2_sqle_connection_domain_controller_delete_by_domain_and_controllers(GSA2SQLEConnection *self, gchar *domain, GPtrArray *controllers, GError **error) {
    gint ret = SQLITE_OK;
    g_autoptr(sqlite3_stmt) statement = NULL;
    if ((ret = sqle_connection_prepare_v2(SQLE_CONNECTION(self)->object, "DELETE FROM domain_controller WHERE domain = ?1 AND controller NOT IN carray(?2, ?3, 'char*')", -1, &statement, NULL, error)) != SQLITE_OK) return ret;
    if ((ret = sqle_statement_bind_text(statement, 1, domain, -1, SQLITE_TRANSIENT, error)) != SQLITE_OK) return ret;
    if ((ret = sqle_carray_bind_pointer(statement, 2, controllers->pdata, SQLITE_STATIC, error)) != SQLITE_OK) return ret;
    if ((ret = sqle_statement_bind_int(statement, 3, controllers->len, error)) != SQLITE_OK) return ret;
    if ((ret = sqle_statement_step(statement, error)) != SQLITE_DONE) return ret;
    return ret;
}

gint gsa2_sqle_connection_domain_controller_select(GSA2SQLEConnection *self, gchar *tail, GList **domain_controllers, GError **error) {
    gint ret = SQLITE_OK;
    g_autofree gchar *sql = g_strjoin(" ", "SELECT * FROM domain_controller", tail, NULL);
    g_autoptr(sqlite3_stmt) statement = NULL;
    if ((ret = sqle_connection_prepare_v2(SQLE_CONNECTION(self)->object, sql, -1, &statement, NULL, error)) != SQLITE_OK) return ret;
    if ((ret = sqle_statement_execute(statement, domain_controllers, gsa2_sqle_statement_domain_controller, NULL, error)) != SQLITE_DONE) return ret;
    return ret;
}

gint gsa2_sqle_connection_domain_controller_preupdate(GSA2SQLEConnection *self, gboolean new, GSA2DomainController **domain_controller, GError **error) {
    gint ret = SQLITE_OK;

    sqlite3_value *value_domain = NULL;
    if ((ret = sqle_connection_preupdate_value(SQLE_CONNECTION(self)->object, new, 0, &value_domain, error)) != SQLITE_OK) return ret;

    sqlite3_value *value_controller = NULL;
    if ((ret = sqle_connection_preupdate_value(SQLE_CONNECTION(self)->object, new, 1, &value_controller, error)) != SQLITE_OK) return ret;

    sqlite3_value *value_key = NULL;
    if ((ret = sqle_connection_preupdate_value(SQLE_CONNECTION(self)->object, new, 2, &value_key, error)) != SQLITE_OK) return ret;

    sqlite3_value *value_ip = NULL;
    if ((ret = sqle_connection_preupdate_value(SQLE_CONNECTION(self)->object, new, 3, &value_ip, error)) != SQLITE_OK) return ret;

    sqlite3_value *value_port = NULL;
    if ((ret = sqle_connection_preupdate_value(SQLE_CONNECTION(self)->object, new, 4, &value_port, error)) != SQLITE_OK) return ret;

    sqlite3_value *value_bssid = NULL;
    if ((ret = sqle_connection_preupdate_value(SQLE_CONNECTION(self)->object, new, 5, &value_bssid, error)) != SQLITE_OK) return ret;

    sqlite3_value *value_verified = NULL;
    if ((ret = sqle_connection_preupdate_value(SQLE_CONNECTION(self)->object, new, 6, &value_verified, error)) != SQLITE_OK) return ret;

    GSA2DomainController ret_domain_controller = {0};
    ret_domain_controller.domain = (gchar *)sqlite3_value_text(value_domain);
    ret_domain_controller.controller = (gchar *)sqlite3_value_text(value_controller);
    ret_domain_controller.key = (gchar *)sqlite3_value_text(value_key);
    ret_domain_controller.ip = (gchar *)sqlite3_value_text(value_ip);
    ret_domain_controller.port = sqlite3_value_int(value_port);
    ret_domain_controller.bssid = (gchar *)sqlite3_value_text(value_bssid);
    ret_domain_controller.verified = sqlite3_value_int(value_verified);

    GE_SET_VALUE(domain_controller, gsa2_domain_controller_copy(&ret_domain_controller));
    return ret;
}

gint gsa2_sqle_connection_identity_upsert(GSA2SQLEConnection *self, GSA2Identity *identity, GError **error) {
    gint ret = SQLITE_OK;
    g_autoptr(sqlite3_stmt) statement = NULL;
    if ((ret = sqle_connection_prepare_v2(SQLE_CONNECTION(self)->object, "INSERT INTO identity (type, value, account, controller) VALUES (?1, ?2, ?3, ?4) ON CONFLICT (type, value) DO UPDATE SET account = ?3, controller = ?4", -1, &statement, NULL, error)) != SQLITE_OK) return ret;
    if ((ret = gsa2_sqle_statement_bind_identity(statement, identity, error)) != SQLITE_OK) return ret;
    if ((ret = sqle_statement_step(statement, error)) != SQLITE_DONE) return ret;
    return ret;
}

gint gsa2_sqle_connection_identity_select(GSA2SQLEConnection *self, gchar *tail, GList **identities, GError **error) {
    gint ret = SQLITE_OK;
    g_autofree gchar *sql = g_strjoin(" ", "SELECT * FROM identity", tail, NULL);
    g_autoptr(sqlite3_stmt) statement = NULL;
    if ((ret = sqle_connection_prepare_v2(SQLE_CONNECTION(self)->object, sql, -1, &statement, NULL, error)) != SQLITE_OK) return ret;
    if ((ret = sqle_statement_execute(statement, identities, gsa2_sqle_statement_identity, NULL, error)) != SQLITE_DONE) return ret;
    return ret;
}

gint gsa2_sqle_connection_invitation_upsert(GSA2SQLEConnection *self, GSA2Invitation *invitation, GError **error) {
    gint ret = SQLITE_OK;
    g_autoptr(sqlite3_stmt) statement = NULL;
    if ((ret = sqle_connection_prepare_v2(SQLE_CONNECTION(self)->object, "INSERT INTO invitation (id, domain, name, expiration, access) VALUES (?1, ?2, ?3, ?4, ?5) ON CONFLICT (id) DO UPDATE SET domain = ?2, name = ?3, expiration = ?4, access = ?5", -1, &statement, NULL, error)) != SQLITE_OK) return ret;
    if ((ret = gsa2_sqle_statement_bind_invitation(statement, invitation, error)) != SQLITE_OK) return ret;
    if ((ret = sqle_statement_step(statement, error)) != SQLITE_DONE) return ret;
    return ret;
}

gint gsa2_sqle_connection_invitation_delete_by_domain_and_id(GSA2SQLEConnection *self, gchar *domain, gchar *id, GError **error) {
    gint ret = SQLITE_OK;
    g_autoptr(sqlite3_stmt) statement = NULL;
    if ((ret = sqle_connection_prepare_v2(SQLE_CONNECTION(self)->object, "DELETE FROM invitation WHERE domain = ?1 AND id = ?2", -1, &statement, NULL, error)) != SQLITE_OK) return ret;
    if ((ret = sqle_statement_bind_text(statement, 1, domain, -1, SQLITE_TRANSIENT, error)) != SQLITE_OK) return ret;
    if ((ret = sqle_statement_bind_text(statement, 2, id, -1, SQLITE_TRANSIENT, error)) != SQLITE_OK) return ret;
    if ((ret = sqle_statement_step(statement, error)) != SQLITE_DONE) return ret;
    return ret;
}

gint gsa2_sqle_connection_invitation_delete_by_domain_and_ids(GSA2SQLEConnection *self, gchar *domain, GPtrArray *ids, GError **error) {
    gint ret = SQLITE_OK;
    g_autoptr(sqlite3_stmt) statement = NULL;
    if ((ret = sqle_connection_prepare_v2(SQLE_CONNECTION(self)->object, "DELETE FROM invitation WHERE domain = ?1 AND id NOT IN carray(?2, ?3, 'char*')", -1, &statement, NULL, error)) != SQLITE_OK) return ret;
    if ((ret = sqle_statement_bind_text(statement, 1, domain, -1, SQLITE_TRANSIENT, error)) != SQLITE_OK) return ret;
    if ((ret = sqle_carray_bind_pointer(statement, 2, ids->pdata, SQLITE_STATIC, error)) != SQLITE_OK) return ret;
    if ((ret = sqle_statement_bind_int(statement, 3, ids->len, error)) != SQLITE_OK) return ret;
    if ((ret = sqle_statement_step(statement, error)) != SQLITE_DONE) return ret;
    return ret;
}

gint gsa2_sqle_connection_invitation_select(GSA2SQLEConnection *self, gchar *tail, GList **invitations, GError **error) {
    gint ret = SQLITE_OK;
    g_autofree gchar *sql = g_strjoin(" ", "SELECT * FROM invitation", tail, NULL);
    g_autoptr(sqlite3_stmt) statement = NULL;
    if ((ret = sqle_connection_prepare_v2(SQLE_CONNECTION(self)->object, sql, -1, &statement, NULL, error)) != SQLITE_OK) return ret;
    if ((ret = sqle_statement_execute(statement, invitations, gsa2_sqle_statement_invitation, NULL, error)) != SQLITE_DONE) return ret;
    return ret;
}

gint gsa2_sqle_connection_token_upsert(GSA2SQLEConnection *self, GSA2Token *token, GError **error) {
    gint ret = SQLITE_OK;
    g_autoptr(sqlite3_stmt) statement = NULL;
    if ((ret = sqle_connection_prepare_v2(SQLE_CONNECTION(self)->object, "INSERT INTO token (subject, id, access, refresh) VALUES (?1, ?2, ?3, ?4) ON CONFLICT (subject, id) DO UPDATE SET access = ?3, refresh = ?4", -1, &statement, NULL, error)) != SQLITE_OK) return ret;
    if ((ret = gsa2_sqle_statement_bind_token(statement, token, error)) != SQLITE_OK) return ret;
    if ((ret = sqle_statement_step(statement, error)) != SQLITE_DONE) return ret;
    return ret;
}

gint gsa2_sqle_connection_token_delete_by_access(GSA2SQLEConnection *self, gchar *access, GError **error) {
    gint ret = SQLITE_OK;
    g_autoptr(sqlite3_stmt) statement = NULL;
    if ((ret = sqle_connection_prepare_v2(SQLE_CONNECTION(self)->object, "DELETE FROM token WHERE access = ?1", -1, &statement, NULL, error)) != SQLITE_OK) return ret;
    if ((ret = sqle_statement_bind_text(statement, 1, access, -1, SQLITE_TRANSIENT, error)) != SQLITE_OK) return ret;
    if ((ret = sqle_statement_step(statement, error)) != SQLITE_DONE) return ret;
    return ret;
}

gint gsa2_sqle_connection_token_select(GSA2SQLEConnection *self, gchar *tail, GList **tokens, GError **error) {
    gint ret = SQLITE_OK;
    g_autofree gchar *sql = g_strjoin(" ", "SELECT * FROM token", tail, NULL);
    g_autoptr(sqlite3_stmt) statement = NULL;
    if ((ret = sqle_connection_prepare_v2(SQLE_CONNECTION(self)->object, sql, -1, &statement, NULL, error)) != SQLITE_OK) return ret;
    if ((ret = sqle_statement_execute(statement, tokens, gsa2_sqle_statement_token, NULL, error)) != SQLITE_DONE) return ret;
    return ret;
}

gint gsa2_sqle_connection_token_select_by_access(GSA2SQLEConnection *self, gchar *access, GList **tokens, GError **error) {
    gint ret = SQLITE_OK;
    g_autoptr(sqlite3_stmt) statement = NULL;
    if ((ret = sqle_connection_prepare_v2(SQLE_CONNECTION(self)->object, "SELECT * FROM token WHERE access = ?1", -1, &statement, NULL, error)) != SQLITE_OK) return ret;
    if ((ret = sqle_statement_bind_text(statement, 1, access, -1, SQLITE_TRANSIENT, error)) != SQLITE_OK) return ret;
    if ((ret = sqle_statement_execute(statement, tokens, gsa2_sqle_statement_token, NULL, error)) != SQLITE_DONE) return ret;
    return ret;
}

gint gsa2_sqle_connection_account_full_select(GSA2SQLEConnection *self, gchar *tail, GList **accounts_full, GError **error) {
    gint ret = SQLITE_OK;
    g_autofree gchar *sql = g_strjoin(" ", "SELECT * FROM account INNER JOIN account_domain ON account.id = account_domain.account", tail, NULL);
    g_autoptr(sqlite3_stmt) statement = NULL;
    if ((ret = sqle_connection_prepare_v2(SQLE_CONNECTION(self)->object, sql, -1, &statement, NULL, error)) != SQLITE_OK) return ret;
    if ((ret = sqle_statement_execute(statement, accounts_full, gsa2_sqle_statement_account_full, NULL, error)) != SQLITE_DONE) return ret;
    return ret;
}

gint gsa2_sqle_connection_domain_full_select(GSA2SQLEConnection *self, gchar *tail, GList **domains_full, GError **error) {
    gint ret = SQLITE_OK;
    g_autofree gchar *sql = g_strjoin(" ", "SELECT * FROM domain INNER JOIN account_domain ON domain.id = account_domain.domain", tail, NULL);
    g_autoptr(sqlite3_stmt) statement = NULL;
    if ((ret = sqle_connection_prepare_v2(SQLE_CONNECTION(self)->object, sql, -1, &statement, NULL, error)) != SQLITE_OK) return ret;
    if ((ret = sqle_statement_execute(statement, domains_full, gsa2_sqle_statement_domain_full, NULL, error)) != SQLITE_DONE) return ret;
    return ret;
}

gint gsa2_sqle_connection_controller_full_select(GSA2SQLEConnection *self, gchar *tail, GList **controllers_full, GError **error) {
    gint ret = SQLITE_OK;
    g_autofree gchar *sql = g_strjoin(" ", "SELECT * FROM controller INNER JOIN domain_controller ON controller.id = domain_controller.controller", tail, NULL);
    g_autoptr(sqlite3_stmt) statement = NULL;
    if ((ret = sqle_connection_prepare_v2(SQLE_CONNECTION(self)->object, sql, -1, &statement, NULL, error)) != SQLITE_OK) return ret;
    if ((ret = sqle_statement_execute(statement, controllers_full, gsa2_sqle_statement_controller_full, NULL, error)) != SQLITE_DONE) return ret;
    return ret;
}
