//
// Created by Dan on 23.02.2025.
//

#ifndef LIBRARY_GS_AUTH_2_GSA2SQLEDATABASE_H
#define LIBRARY_GS_AUTH_2_GSA2SQLEDATABASE_H

#include "gsa2main.h"

G_BEGIN_DECLS

extern gchar gsa2_sqle_database_data[];
extern gint gsa2_sqle_database_n;

gboolean gsa2_sqle_database_set(gchar *file, GError **error);

G_END_DECLS

#endif //LIBRARY_GS_AUTH_2_GSA2SQLEDATABASE_H
