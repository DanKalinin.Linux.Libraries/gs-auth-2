//
// Created by root on 27.05.2020.
//

#include "gsa2schema.h"










GSA2Account *gsa2_account_copy(GSA2Account *self) {
    GSA2Account *ret = g_new0(GSA2Account, 1);
    gsa2_account_set_id(ret, self->id);
    gsa2_account_set_name(ret, self->name);
    gsa2_account_set_password(ret, self->password);
    return ret;
}

void gsa2_account_free(GSA2Account *self) {
    gsa2_account_set_id(self, NULL);
    gsa2_account_set_name(self, NULL);
    gsa2_account_set_password(self, NULL);
    g_free(self);
}










GSA2Domain *gsa2_domain_copy(GSA2Domain *self) {
    GSA2Domain *ret = g_new0(GSA2Domain, 1);
    gsa2_domain_set_id(ret, self->id);
    gsa2_domain_set_owner(ret, self->owner);
    gsa2_domain_set_name(ret, self->name);
    return ret;
}

void gsa2_domain_free(GSA2Domain *self) {
    gsa2_domain_set_id(self, NULL);
    gsa2_domain_set_owner(self, NULL);
    gsa2_domain_set_name(self, NULL);
    g_free(self);
}










GSA2Controller *gsa2_controller_copy(GSA2Controller *self) {
    GSA2Controller *ret = g_new0(GSA2Controller, 1);
    gsa2_controller_set_id(ret, self->id);
    gsa2_controller_set_mac(ret, self->mac);
    gsa2_controller_set_model(ret, self->model);
    gsa2_controller_set_serial(ret, self->serial);
    return ret;
}

void gsa2_controller_free(GSA2Controller *self) {
    gsa2_controller_set_id(self, NULL);
    gsa2_controller_set_mac(self, NULL);
    gsa2_controller_set_model(self, NULL);
    gsa2_controller_set_serial(self, NULL);
    g_free(self);
}










GSA2AccountDomain *gsa2_account_domain_copy(GSA2AccountDomain *self) {
    GSA2AccountDomain *ret = g_new0(GSA2AccountDomain, 1);
    gsa2_account_domain_set_account(ret, self->account);
    gsa2_account_domain_set_domain(ret, self->domain);
    gsa2_account_domain_set_expiration(ret, self->expiration);
    return ret;
}

void gsa2_account_domain_free(GSA2AccountDomain *self) {
    gsa2_account_domain_set_account(self, NULL);
    gsa2_account_domain_set_domain(self, NULL);
    gsa2_account_domain_set_expiration(self, NULL);
    g_free(self);
}










GSA2DomainController *gsa2_domain_controller_copy(GSA2DomainController *self) {
    GSA2DomainController *ret = g_new0(GSA2DomainController, 1);
    gsa2_domain_controller_set_domain(ret, self->domain);
    gsa2_domain_controller_set_controller(ret, self->controller);
    gsa2_domain_controller_set_key(ret, self->key);
    gsa2_domain_controller_set_ip(ret, self->ip);
    ret->port = self->port;
    gsa2_domain_controller_set_bssid(ret, self->bssid);
    ret->verified = self->verified;
    return ret;
}

void gsa2_domain_controller_free(GSA2DomainController *self) {
    gsa2_domain_controller_set_domain(self, NULL);
    gsa2_domain_controller_set_controller(self, NULL);
    gsa2_domain_controller_set_key(self, NULL);
    gsa2_domain_controller_set_ip(self, NULL);
    gsa2_domain_controller_set_bssid(self, NULL);
    g_free(self);
}










GSA2Identity *gsa2_identity_copy(GSA2Identity *self) {
    GSA2Identity *ret = g_new0(GSA2Identity, 1);
    gsa2_identity_set_type(ret, self->type);
    gsa2_identity_set_value(ret, self->value);
    gsa2_identity_set_account(ret, self->account);
    gsa2_identity_set_controller(ret, self->controller);
    return ret;
}

void gsa2_identity_free(GSA2Identity *self) {
    gsa2_identity_set_type(self, NULL);
    gsa2_identity_set_value(self, NULL);
    gsa2_identity_set_account(self, NULL);
    gsa2_identity_set_controller(self, NULL);
    g_free(self);
}










GSA2Invitation *gsa2_invitation_copy(GSA2Invitation *self) {
    GSA2Invitation *ret = g_new0(GSA2Invitation, 1);
    gsa2_invitation_set_id(ret, self->id);
    gsa2_invitation_set_domain(ret, self->domain);
    gsa2_invitation_set_name(ret, self->name);
    gsa2_invitation_set_expiration(ret, self->expiration);
    return ret;
}

void gsa2_invitation_free(GSA2Invitation *self) {
    gsa2_invitation_set_id(self, NULL);
    gsa2_invitation_set_domain(self, NULL);
    gsa2_invitation_set_name(self, NULL);
    gsa2_invitation_set_expiration(self, NULL);
    g_free(self);
}










GSA2Token *gsa2_token_copy(GSA2Token *self) {
    GSA2Token *ret = g_new0(GSA2Token, 1);
    gsa2_token_set_subject(ret, self->subject);
    gsa2_token_set_id(ret, self->id);
    gsa2_token_set_access(ret, self->access);
    gsa2_token_set_refresh(ret, self->refresh);
    return ret;
}

void gsa2_token_free(GSA2Token *self) {
    gsa2_token_set_subject(self, NULL);
    gsa2_token_set_id(self, NULL);
    gsa2_token_set_access(self, NULL);
    gsa2_token_set_refresh(self, NULL);
    g_free(self);
}










GSA2Credential *gsa2_credential_copy(GSA2Credential *self) {
    GSA2Credential *ret = g_new0(GSA2Credential, 1);
    gsa2_credential_set_type(ret, self->type);
    gsa2_credential_set_value(ret, self->value);
    return ret;
}

void gsa2_credential_free(GSA2Credential *self) {
    gsa2_credential_set_type(self, NULL);
    gsa2_credential_set_value(self, NULL);
    g_free(self);
}










GSA2AccountFull *gsa2_account_full_copy(GSA2AccountFull *self) {
    GSA2AccountFull *ret = g_new0(GSA2AccountFull, 1);
    gsa2_account_full_set_account(ret, self->account);
    gsa2_account_full_set_account_domain(ret, self->account_domain);
    return ret;
}

void gsa2_account_full_free(GSA2AccountFull *self) {
    gsa2_account_full_set_account(self, NULL);
    gsa2_account_full_set_account_domain(self, NULL);
    g_free(self);
}










GSA2DomainFull *gsa2_domain_full_copy(GSA2DomainFull *self) {
    GSA2DomainFull *ret = g_new0(GSA2DomainFull, 1);
    gsa2_domain_full_set_domain(ret, self->domain);
    gsa2_domain_full_set_account_domain(ret, self->account_domain);
    return ret;
}

void gsa2_domain_full_free(GSA2DomainFull *self) {
    gsa2_domain_full_set_domain(self, NULL);
    gsa2_domain_full_set_account_domain(self, NULL);
    g_free(self);
}










GSA2ControllerFull *gsa2_controller_full_copy(GSA2ControllerFull *self) {
    GSA2ControllerFull *ret = g_new0(GSA2ControllerFull, 1);
    gsa2_controller_full_set_controller(ret, self->controller);
    gsa2_controller_full_set_domain_controller(ret, self->domain_controller);
    return ret;
}

void gsa2_controller_full_free(GSA2ControllerFull *self) {
    gsa2_controller_full_set_controller(self, NULL);
    gsa2_controller_full_set_domain_controller(self, NULL);
    g_free(self);
}
