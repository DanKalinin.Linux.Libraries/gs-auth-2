//
// Created by root on 05.06.2020.
//

#include "gsa2jsebuilder.h"

JsonObject *gsa2_jse_builder_identity_check(GSA2Identity *identity) {
    JsonObject *object_identity = gsa2_jse_builder_identity(identity);

    JsonObject *ret = json_object_new();
    json_object_set_object_member(ret, "identity", object_identity);
    return ret;
}

JsonObject *gsa2_jse_builder_code_send(GSA2Identity *identity) {
    JsonObject *object_identity = gsa2_jse_builder_identity(identity);

    JsonObject *ret = json_object_new();
    json_object_set_object_member(ret, "identity", object_identity);
    return ret;
}

JsonObject *gsa2_jse_builder_account_create(GSA2Account *account, GList *identities) {
    JsonObject *ret = gsa2_jse_builder_account(account, identities);
    return ret;
}

JsonObject *gsa2_jse_builder_password_update(gchar *password) {
    JsonObject *ret = json_object_new();
    json_object_set_string_member(ret, "password", password);
    return ret;
}

JsonObject *gsa2_jse_builder_domain_create(GSA2Domain *domain, GList *controllers_full) {
    JsonObject *ret = gsa2_jse_builder_domain(domain, controllers_full);
    return ret;
}

JsonObject *gsa2_jse_builder_domain_rename(gchar *name) {
    JsonObject *ret = json_object_new();
    json_object_set_string_member(ret, "name", name);
    return ret;
}

JsonObject *gsa2_jse_builder_invitation_create(GSA2Invitation *invitation) {
    JsonObject *ret = gsa2_jse_builder_invitation(invitation);
    return ret;
}

JsonObject *gsa2_jse_builder_invitation_accept(gchar *invitation_id) {
    JsonObject *ret = json_object_new();
    json_object_set_string_member(ret, "id", invitation_id);
    return ret;
}

JsonObject *gsa2_jse_builder_controllers_add(GList *controllers_full) {
    JsonArray *array_controllers = gsa2_jse_builder_controllers_full(controllers_full);

    JsonObject *ret = json_object_new();
    json_object_set_array_member(ret, "controllers", array_controllers);
    return ret;
}

JsonObject *gsa2_jse_builder_controller_create(GSA2Controller *controller) {
    JsonObject *ret = gsa2_jse_builder_controller(controller);
    return ret;
}

JsonObject *gsa2_jse_builder_controller_verify(gchar *code) {
    JsonObject *ret = json_object_new();
    json_object_set_string_member(ret, "code", code);
    return ret;
}

JsonObject *gsa2_jse_builder_token_issue(GSA2Identity *identity, GSA2Credential *credential) {
    JsonObject *object_identity = gsa2_jse_builder_identity(identity);
    JsonObject *object_credential = gsa2_jse_builder_credential(credential);

    JsonObject *ret = json_object_new();
    json_object_set_object_member(ret, "identity", object_identity);
    json_object_set_object_member(ret, "credential", object_credential);
    return ret;
}

JsonObject *gsa2_jse_builder_token_refresh(gchar *refresh) {
    JsonObject *ret = json_object_new();
    json_object_set_string_member(ret, "refreshToken", refresh);
    return ret;
}

JsonObject *gsa2_jse_builder_token_revoke(gchar *access) {
    JsonObject *ret = json_object_new();
    json_object_set_string_member(ret, "accessToken", access);
    return ret;
}

JsonObject *gsa2_jse_builder_camera_playlist(gchar *key) {
    JsonObject *ret = json_object_new();
    json_object_set_string_member(ret, "access_key", key);
    return ret;
}

JsonObject *gsa2_jse_builder_account(GSA2Account *account, GList *identities) {
    JsonArray *array_identities = gsa2_jse_builder_identities(identities);

    JsonObject *object_credentials = json_object_new();
    json_object_set_string_member(object_credentials, "password", account->password);
    json_object_set_array_member(object_credentials, "identities", array_identities);

    JsonObject *ret = json_object_new();
    json_object_set_string_member(ret, "name", account->name);
    json_object_set_object_member(ret, "credentials", object_credentials);
    return ret;
}

JsonObject *gsa2_jse_builder_domain(GSA2Domain *domain, GList *controllers_full) {
    JsonArray *array_controllers = gsa2_jse_builder_controllers_full(controllers_full);

    JsonObject *ret = json_object_new();
    json_object_set_string_member(ret, "name", domain->name);
    json_object_set_array_member(ret, "controllers", array_controllers);
    return ret;
}

JsonObject *gsa2_jse_builder_controller(GSA2Controller *controller) {
    JsonObject *ret = json_object_new();
    json_object_set_string_member(ret, "macAddress", controller->mac);
    json_object_set_string_member(ret, "model", controller->model);
    json_object_set_string_member(ret, "serial", controller->serial);
    return ret;
}

JsonObject *gsa2_jse_builder_controller_full(GSA2ControllerFull *controller_full) {
    JsonObject *ret = json_object_new();
    json_object_set_string_member(ret, "macAddress", controller_full->controller->mac);
    json_object_set_string_member(ret, "model", controller_full->controller->model);
    json_object_set_string_member(ret, "serial", controller_full->controller->serial);
    json_object_set_string_member(ret, "localAccessKey", controller_full->domain_controller->key);
    json_object_set_string_member(ret, "ip", controller_full->domain_controller->ip);
    json_object_set_int_member(ret, "port", controller_full->domain_controller->port);
    json_object_set_string_member(ret, "bssid", controller_full->domain_controller->bssid);
    return ret;
}

JsonArray *gsa2_jse_builder_controllers_full(GList *controllers_full) {
    JsonArray *ret = json_array_new();

    GList *controller_full = NULL;
    for (controller_full = controllers_full; controller_full != NULL; controller_full = controller_full->next) {
        JsonObject *object_controller_full = gsa2_jse_builder_controller_full(controller_full->data);
        json_array_add_object_element(ret, object_controller_full);
    }

    return ret;
}

JsonObject *gsa2_jse_builder_identity(GSA2Identity *identity) {
    JsonObject *ret = json_object_new();
    json_object_set_string_member(ret, "type", identity->type);
    json_object_set_string_member(ret, "value", identity->value);
    return ret;
}

JsonArray *gsa2_jse_builder_identities(GList *identities) {
    JsonArray *ret = json_array_new();

    GList *identity = NULL;
    for (identity = identities; identity != NULL; identity = identity->next) {
        JsonObject *object_identity = gsa2_jse_builder_identity(identity->data);
        json_array_add_object_element(ret, object_identity);
    }

    return ret;
}

JsonObject *gsa2_jse_builder_invitation(GSA2Invitation *invitation) {
    JsonObject *ret = json_object_new();
    json_object_set_string_member(ret, "name", invitation->name);
    json_object_set_int_member(ret, "expiresIn", strtol(invitation->expiration, NULL, 10));
    json_object_set_int_member(ret, "accessExpiresIn", invitation->access);
    return ret;
}

JsonObject *gsa2_jse_builder_credential(GSA2Credential *credential) {
    JsonObject *ret = json_object_new();
    json_object_set_string_member(ret, "type", credential->type);
    json_object_set_string_member(ret, "value", credential->value);
    return ret;
}
