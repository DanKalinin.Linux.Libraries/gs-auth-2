//
// Created by root on 01.06.2020.
//

#ifndef LIBRARY_GS_AUTH_2_GSA2MAIN_H
#define LIBRARY_GS_AUTH_2_GSA2MAIN_H

#include <glib-ext/glib-ext.h>
#include <sqlite-ext/sqlite-ext.h>
#include <soup-ext/soup-ext.h>
#include <json-ext/json-ext.h>
#include <jwt-ext/jwt-ext.h>
#include <glib-networking-ext/glib-networking-ext.h>

#endif //LIBRARY_GS_AUTH_2_GSA2MAIN_H
