//
// Created by root on 03.06.2020.
//

#ifndef LIBRARY_GS_AUTH_2_GSA2SOEMESSAGE_H
#define LIBRARY_GS_AUTH_2_GSA2SOEMESSAGE_H

#include "gsa2main.h"
#include "gsa2soemessageheaders.h"
#include "gsa2schema.h"
#include "gsa2sqleconnection.h"
#include "gsa2jsebuilder.h"
#include "gsa2jsegenerator.h"
#include "gsa2jsereader.h"
#include "gsa2jseparser.h"

G_BEGIN_DECLS

#define GSA2_TYPE_SOE_MESSAGE gsa2_soe_message_get_type()

GE_DECLARE_DERIVABLE_TYPE(GSA2SOEMessage, gsa2_soe_message, GSA2, SOE_MESSAGE, SOEMessage)

struct _GSA2SOEMessageClass {
    SOEMessageClass super;
};

struct _GSA2SOEMessage {
    SOEMessage super;
};

GSA2SOEMessage *gsa2_soe_message_identity_check(SoupURI *base, GSA2Identity *identity);
gboolean gsa2_soe_message_identity_check_finish(GSA2SOEMessage *self, gboolean *available, GError **error);

GSA2SOEMessage *gsa2_soe_message_code_send(SoupURI *base, GSA2Identity *identity);
GDateTime *gsa2_soe_message_code_send_finish(GSA2SOEMessage *self, GError **error);

GSA2SOEMessage *gsa2_soe_message_account_create(SoupURI *base, GSA2Account *account, GList *identities);
GSA2Account *gsa2_soe_message_account_create_finish(GSA2SOEMessage *self, GSA2SQLEConnection *connection, GDateTime **expiration, GError **error);

GSA2SOEMessage *gsa2_soe_message_account_get(SoupURI *base, gchar *access);
GSA2Account *gsa2_soe_message_account_get_finish(GSA2SOEMessage *self, GSA2SQLEConnection *connection, GError **error);

GSA2SOEMessage *gsa2_soe_message_password_update(SoupURI *base, gchar *password, gchar *access);
gboolean gsa2_soe_message_password_update_finish(GSA2SOEMessage *self, GError **error);

GSA2SOEMessage *gsa2_soe_message_domain_create(SoupURI *base, GSA2Domain *domain, GList *controllers_full, gchar *access);
GSA2DomainFull *gsa2_soe_message_domain_create_finish(GSA2SOEMessage *self, GSA2SQLEConnection *connection, GError **error);

GSA2SOEMessage *gsa2_soe_message_domain_rename(SoupURI *base, gchar *domain_id, gchar *name, gchar *access);
gboolean gsa2_soe_message_domain_rename_finish(GSA2SOEMessage *self, GSA2SQLEConnection *connection, GError **error);

GSA2SOEMessage *gsa2_soe_message_domain_delete(SoupURI *base, gchar *domain_id, gchar *access);
gboolean gsa2_soe_message_domain_delete_finish(GSA2SOEMessage *self, GSA2SQLEConnection *connection, GError **error);

GSA2SOEMessage *gsa2_soe_message_domain_exit(SoupURI *base, gchar *domain_id, gchar *access);
gboolean gsa2_soe_message_domain_exit_finish(GSA2SOEMessage *self, GSA2SQLEConnection *connection, GError **error);

GSA2SOEMessage *gsa2_soe_message_guest_delete(SoupURI *base, gchar *guest_id, gchar *domain_id, gchar *access);
GSA2DomainFull *gsa2_soe_message_guest_delete_finish(GSA2SOEMessage *self, GSA2SQLEConnection *connection, GError **error);

GSA2SOEMessage *gsa2_soe_message_invitation_create(SoupURI *base, GSA2Invitation *invitation, gchar *domain_id, gchar *access);
GSA2Invitation *gsa2_soe_message_invitation_create_finish(GSA2SOEMessage *self, GSA2SQLEConnection *connection, GError **error);

GSA2SOEMessage *gsa2_soe_message_invitation_revoke(SoupURI *base, gchar *invitation_id, gchar *domain_id, gchar *access);
gboolean gsa2_soe_message_invitation_revoke_finish(GSA2SOEMessage *self, GSA2SQLEConnection *connection, GError **error);

GSA2SOEMessage *gsa2_soe_message_invitation_accept(SoupURI *base, gchar *invitation_id, gchar *access);
GSA2DomainFull *gsa2_soe_message_invitation_accept_finish(GSA2SOEMessage *self, GSA2SQLEConnection *connection, GError **error);

GSA2SOEMessage *gsa2_soe_message_code_get(SoupURI *base, gchar *domain_id, gchar *access);
gchar *gsa2_soe_message_code_get_finish(GSA2SOEMessage *self, GError **error);

GSA2SOEMessage *gsa2_soe_message_controllers_add(SoupURI *base, GList *controllers_full, gchar *domain_id, gchar *access);
gboolean gsa2_soe_message_controllers_add_finish(GSA2SOEMessage *self, GSA2SQLEConnection *connection, GList **controllers_full, GError **error);

GSA2SOEMessage *gsa2_soe_message_controller_remove(SoupURI *base, gchar *controller_id, gchar *domain_id, gchar *access);
gboolean gsa2_soe_message_controller_remove_finish(GSA2SOEMessage *self, GSA2SQLEConnection *connection, GError **error);

GSA2SOEMessage *gsa2_soe_message_controller_create(SoupURI *base, GSA2Controller *controller);
GSA2Controller *gsa2_soe_message_controller_create_finish(GSA2SOEMessage *self, GSA2SQLEConnection *connection, GError **error);

GSA2SOEMessage *gsa2_soe_message_controller_verify(SoupURI *base, gchar *code, gchar *access);
gboolean gsa2_soe_message_controller_verify_finish(GSA2SOEMessage *self, GSA2SQLEConnection *connection, GError **error);

GSA2SOEMessage *gsa2_soe_message_token_issue(SoupURI *base, GSA2Identity *identity, GSA2Credential *credential);
GSA2Token *gsa2_soe_message_token_issue_finish(GSA2SOEMessage *self, GSA2SQLEConnection *connection, GError **error);

GSA2SOEMessage *gsa2_soe_message_token_refresh(SoupURI *base, gchar *refresh);
GSA2Token *gsa2_soe_message_token_refresh_finish(GSA2SOEMessage *self, GSA2SQLEConnection *connection, GError **error);

GSA2SOEMessage *gsa2_soe_message_token_revoke(SoupURI *base, gchar *access);
gboolean gsa2_soe_message_token_revoke_finish(GSA2SOEMessage *self, GSA2SQLEConnection *connection, GError **error);

GSA2SOEMessage *gsa2_soe_message_camera_playlist(SoupURI *base, gchar *id, gchar *key, gchar *controller, gchar *access);
gchar *gsa2_soe_message_camera_playlist_finish(GSA2SOEMessage *self, GError **error);

GSA2SOEMessage *gsa2_soe_message_camera_heartbeat(SoupURI *base, gchar *id, gchar *controller, gchar *access);
gboolean gsa2_soe_message_camera_heartbeat_finish(GSA2SOEMessage *self, GError **error);

G_END_DECLS

#endif //LIBRARY_GS_AUTH_2_GSA2SOEMESSAGE_H
