//
// Created by root on 31.05.2020.
//

#include "gsa2sqlestatement.h"

gint gsa2_sqle_statement_bind_account(sqlite3_stmt *self, GSA2Account *account, GError **error) {
    gint ret = SQLITE_OK;
    if ((ret = sqle_statement_bind_text(self, 1, account->id, -1, SQLITE_TRANSIENT, error)) != SQLITE_OK) return ret;
    if ((ret = sqle_statement_bind_text(self, 2, account->name, -1, SQLITE_TRANSIENT, error)) != SQLITE_OK) return ret;
    if ((ret = sqle_statement_bind_text(self, 3, account->password, -1, SQLITE_TRANSIENT, error)) != SQLITE_OK) return ret;
    return ret;
}

gpointer gsa2_sqle_statement_account(sqlite3_stmt *self) {
    GSA2Account ret = {0};
    ret.id = (gchar *)sqlite3_column_text(self, 0);
    ret.name = (gchar *)sqlite3_column_text(self, 1);
    ret.password = (gchar *)sqlite3_column_text(self, 2);
    return gsa2_account_copy(&ret);
}

gint gsa2_sqle_statement_bind_domain(sqlite3_stmt *self, GSA2Domain *domain, GError **error) {
    gint ret = SQLITE_OK;
    if ((ret = sqle_statement_bind_text(self, 1, domain->id, -1, SQLITE_TRANSIENT, error)) != SQLITE_OK) return ret;
    if ((ret = sqle_statement_bind_text(self, 2, domain->owner, -1, SQLITE_TRANSIENT, error)) != SQLITE_OK) return ret;
    if ((ret = sqle_statement_bind_text(self, 3, domain->name, -1, SQLITE_TRANSIENT, error)) != SQLITE_OK) return ret;
    return ret;
}

gpointer gsa2_sqle_statement_domain(sqlite3_stmt *self) {
    GSA2Domain ret = {0};
    ret.id = (gchar *)sqlite3_column_text(self, 0);
    ret.owner = (gchar *)sqlite3_column_text(self, 1);
    ret.name = (gchar *)sqlite3_column_text(self, 2);
    return gsa2_domain_copy(&ret);
}

gint gsa2_sqle_statement_bind_controller(sqlite3_stmt *self, GSA2Controller *controller, GError **error) {
    gint ret = SQLITE_OK;
    if ((ret = sqle_statement_bind_text(self, 1, controller->id, -1, SQLITE_TRANSIENT, error)) != SQLITE_OK) return ret;
    if ((ret = sqle_statement_bind_text(self, 2, controller->mac, -1, SQLITE_TRANSIENT, error)) != SQLITE_OK) return ret;
    if ((ret = sqle_statement_bind_text(self, 3, controller->model, -1, SQLITE_TRANSIENT, error)) != SQLITE_OK) return ret;
    if ((ret = sqle_statement_bind_text(self, 4, controller->serial, -1, SQLITE_TRANSIENT, error)) != SQLITE_OK) return ret;
    return ret;
}

gpointer gsa2_sqle_statement_controller(sqlite3_stmt *self) {
    GSA2Controller ret = {0};
    ret.id = (gchar *)sqlite3_column_text(self, 0);
    ret.mac = (gchar *)sqlite3_column_text(self, 1);
    ret.model = (gchar *)sqlite3_column_text(self, 2);
    ret.serial = (gchar *)sqlite3_column_text(self, 3);
    return gsa2_controller_copy(&ret);
}

gint gsa2_sqle_statement_bind_account_domain(sqlite3_stmt *self, GSA2AccountDomain *account_domain, GError **error) {
    gint ret = SQLITE_OK;
    if ((ret = sqle_statement_bind_text(self, 1, account_domain->account, -1, SQLITE_TRANSIENT, error)) != SQLITE_OK) return ret;
    if ((ret = sqle_statement_bind_text(self, 2, account_domain->domain, -1, SQLITE_TRANSIENT, error)) != SQLITE_OK) return ret;
    if ((ret = sqle_statement_bind_text(self, 3, account_domain->expiration, -1, SQLITE_TRANSIENT, error)) != SQLITE_OK) return ret;
    if ((ret = sqle_statement_bind_int(self, 4, account_domain->current, error)) != SQLITE_OK) return ret;
    return ret;
}

gpointer gsa2_sqle_statement_account_domain(sqlite3_stmt *self) {
    GSA2AccountDomain ret = {0};
    ret.account = (gchar *)sqlite3_column_text(self, 0);
    ret.domain = (gchar *)sqlite3_column_text(self, 1);
    ret.expiration = (gchar *)sqlite3_column_text(self, 2);
    ret.current = sqlite3_column_int(self, 3);
    return gsa2_account_domain_copy(&ret);
}

gint gsa2_sqle_statement_bind_domain_controller(sqlite3_stmt *self, GSA2DomainController *domain_controller, GError **error) {
    gint ret = SQLITE_OK;
    if ((ret = sqle_statement_bind_text(self, 1, domain_controller->domain, -1, SQLITE_TRANSIENT, error)) != SQLITE_OK) return ret;
    if ((ret = sqle_statement_bind_text(self, 2, domain_controller->controller, -1, SQLITE_TRANSIENT, error)) != SQLITE_OK) return ret;
    if ((ret = sqle_statement_bind_text(self, 3, domain_controller->key, -1, SQLITE_TRANSIENT, error)) != SQLITE_OK) return ret;
    if ((ret = sqle_statement_bind_text(self, 4, domain_controller->ip, -1, SQLITE_TRANSIENT, error)) != SQLITE_OK) return ret;
    if ((ret = sqle_statement_bind_int(self, 5, domain_controller->port, error)) != SQLITE_OK) return ret;
    if ((ret = sqle_statement_bind_text(self, 6, domain_controller->bssid, -1, SQLITE_TRANSIENT, error)) != SQLITE_OK) return ret;
    if ((ret = sqle_statement_bind_int(self, 7, domain_controller->verified, error)) != SQLITE_OK) return ret;
    return ret;
}

gpointer gsa2_sqle_statement_domain_controller(sqlite3_stmt *self) {
    GSA2DomainController ret = {0};
    ret.domain = (gchar *)sqlite3_column_text(self, 0);
    ret.controller = (gchar *)sqlite3_column_text(self, 1);
    ret.key = (gchar *)sqlite3_column_text(self, 2);
    ret.ip = (gchar *)sqlite3_column_text(self, 3);
    ret.port = sqlite3_column_int(self, 4);
    ret.bssid = (gchar *)sqlite3_column_text(self, 5);
    ret.verified = sqlite3_column_int(self, 6);
    return gsa2_domain_controller_copy(&ret);
}

gint gsa2_sqle_statement_bind_identity(sqlite3_stmt *self, GSA2Identity *identity, GError **error) {
    gint ret = SQLITE_OK;
    if ((ret = sqle_statement_bind_text(self, 1, identity->type, -1, SQLITE_TRANSIENT, error)) != SQLITE_OK) return ret;
    if ((ret = sqle_statement_bind_text(self, 2, identity->value, -1, SQLITE_TRANSIENT, error)) != SQLITE_OK) return ret;
    if ((ret = sqle_statement_bind_text(self, 3, identity->account, -1, SQLITE_TRANSIENT, error)) != SQLITE_OK) return ret;
    if ((ret = sqle_statement_bind_text(self, 4, identity->controller, -1, SQLITE_TRANSIENT, error)) != SQLITE_OK) return ret;
    return ret;
}

gpointer gsa2_sqle_statement_identity(sqlite3_stmt *self) {
    GSA2Identity ret = {0};
    ret.type = (gchar *)sqlite3_column_text(self, 0);
    ret.value = (gchar *)sqlite3_column_text(self, 1);
    ret.account = (gchar *)sqlite3_column_text(self, 2);
    ret.controller = (gchar *)sqlite3_column_text(self, 3);
    return gsa2_identity_copy(&ret);
}

gint gsa2_sqle_statement_bind_invitation(sqlite3_stmt *self, GSA2Invitation *invitation, GError **error) {
    gint ret = SQLITE_OK;
    if ((ret = sqle_statement_bind_text(self, 1, invitation->id, -1, SQLITE_TRANSIENT, error)) != SQLITE_OK) return ret;
    if ((ret = sqle_statement_bind_text(self, 2, invitation->domain, -1, SQLITE_TRANSIENT, error)) != SQLITE_OK) return ret;
    if ((ret = sqle_statement_bind_text(self, 3, invitation->name, -1, SQLITE_TRANSIENT, error)) != SQLITE_OK) return ret;
    if ((ret = sqle_statement_bind_text(self, 4, invitation->expiration, -1, SQLITE_TRANSIENT, error)) != SQLITE_OK) return ret;
    if ((ret = sqle_statement_bind_int64(self, 5, invitation->access, error)) != SQLITE_OK) return ret;
    return ret;
}

gpointer gsa2_sqle_statement_invitation(sqlite3_stmt *self) {
    GSA2Invitation ret = {0};
    ret.id = (gchar *)sqlite3_column_text(self, 0);
    ret.domain = (gchar *)sqlite3_column_text(self, 1);
    ret.name = (gchar *)sqlite3_column_text(self, 2);
    ret.expiration = (gchar *)sqlite3_column_text(self, 3);
    ret.access = sqlite3_column_int64(self, 4);
    return gsa2_invitation_copy(&ret);
}

gint gsa2_sqle_statement_bind_token(sqlite3_stmt *self, GSA2Token *token, GError **error) {
    gint ret = SQLITE_OK;
    if ((ret = sqle_statement_bind_text(self, 1, token->subject, -1, SQLITE_TRANSIENT, error)) != SQLITE_OK) return ret;
    if ((ret = sqle_statement_bind_text(self, 2, token->id, -1, SQLITE_TRANSIENT, error)) != SQLITE_OK) return ret;
    if ((ret = sqle_statement_bind_text(self, 3, token->access, -1, SQLITE_TRANSIENT, error)) != SQLITE_OK) return ret;
    if ((ret = sqle_statement_bind_text(self, 4, token->refresh, -1, SQLITE_TRANSIENT, error)) != SQLITE_OK) return ret;
    return ret;
}

gpointer gsa2_sqle_statement_token(sqlite3_stmt *self) {
    GSA2Token ret = {0};
    ret.subject = (gchar *)sqlite3_column_text(self, 0);
    ret.id = (gchar *)sqlite3_column_text(self, 1);
    ret.access = (gchar *)sqlite3_column_text(self, 2);
    ret.refresh = (gchar *)sqlite3_column_text(self, 3);
    return gsa2_token_copy(&ret);
}

gpointer gsa2_sqle_statement_account_full(sqlite3_stmt *self) {
    GSA2Account account = {0};
    account.id = (gchar *)sqlite3_column_text(self, 0);
    account.name = (gchar *)sqlite3_column_text(self, 1);
    account.password = (gchar *)sqlite3_column_text(self, 2);

    GSA2AccountDomain account_domain = {0};
    account_domain.account = (gchar *)sqlite3_column_text(self, 3);
    account_domain.domain = (gchar *)sqlite3_column_text(self, 4);
    account_domain.expiration = (gchar *)sqlite3_column_text(self, 5);
    account_domain.current = sqlite3_column_int(self, 6);

    GSA2AccountFull ret = {0};
    ret.account = &account;
    ret.account_domain = &account_domain;
    return gsa2_account_full_copy(&ret);
}

gpointer gsa2_sqle_statement_domain_full(sqlite3_stmt *self) {
    GSA2Domain domain = {0};
    domain.id = (gchar *)sqlite3_column_text(self, 0);
    domain.owner = (gchar *)sqlite3_column_text(self, 1);
    domain.name = (gchar *)sqlite3_column_text(self, 2);

    GSA2AccountDomain account_domain = {0};
    account_domain.account = (gchar *)sqlite3_column_text(self, 3);
    account_domain.domain = (gchar *)sqlite3_column_text(self, 4);
    account_domain.expiration = (gchar *)sqlite3_column_text(self, 5);
    account_domain.current = sqlite3_column_int(self, 6);

    GSA2DomainFull ret = {0};
    ret.domain = &domain;
    ret.account_domain = &account_domain;
    return gsa2_domain_full_copy(&ret);
}

gpointer gsa2_sqle_statement_controller_full(sqlite3_stmt *self) {
    GSA2Controller controller = {0};
    controller.id = (gchar *)sqlite3_column_text(self, 0);
    controller.mac = (gchar *)sqlite3_column_text(self, 1);
    controller.model = (gchar *)sqlite3_column_text(self, 2);
    controller.serial = (gchar *)sqlite3_column_text(self, 3);

    GSA2DomainController domain_controller = {0};
    domain_controller.domain = (gchar *)sqlite3_column_text(self, 4);
    domain_controller.controller = (gchar *)sqlite3_column_text(self, 5);
    domain_controller.key = (gchar *)sqlite3_column_text(self, 6);
    domain_controller.ip = (gchar *)sqlite3_column_text(self, 7);
    domain_controller.port = sqlite3_column_int(self, 8);
    domain_controller.bssid = (gchar *)sqlite3_column_text(self, 9);
    domain_controller.verified = sqlite3_column_int(self, 10);

    GSA2ControllerFull ret = {0};
    ret.controller = &controller;
    ret.domain_controller = &domain_controller;
    return gsa2_controller_full_copy(&ret);
}
