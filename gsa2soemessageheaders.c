//
// Created by root on 17.11.2020.
//

#include "gsa2soemessageheaders.h"

void gsa2_soe_message_headers_set_x_controller_id(SoupMessageHeaders *self, gchar *id) {
    soup_message_headers_replace(self, "X-Controller-ID", id);
}

void gsa2_soe_message_headers_set_x_legacy_formatting(SoupMessageHeaders *self, gboolean legacy) {
    g_autofree gchar *value = g_strdup_printf("%i", legacy);
    soup_message_headers_replace(self, "X-Legacy-Formatting", value);
}
