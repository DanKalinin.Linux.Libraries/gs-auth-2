//
// Created by root on 19.10.2021.
//

#ifndef LIBRARY_GS_AUTH_2_GSA2INIT_H
#define LIBRARY_GS_AUTH_2_GSA2INIT_H

#include "gsa2main.h"

G_BEGIN_DECLS

void gsa2_init(void);

G_END_DECLS

#endif //LIBRARY_GS_AUTH_2_GSA2INIT_H
