//
// Created by root on 03.06.2020.
//

#include "gsa2soelogger.h"

G_DEFINE_TYPE(GSA2SOELogger, gsa2_soe_logger, SOE_TYPE_LOGGER)

static void gsa2_soe_logger_class_init(GSA2SOELoggerClass *class) {

}

static void gsa2_soe_logger_init(GSA2SOELogger *self) {

}

GSA2SOELogger *gsa2_soe_logger_new(void) {
    GSA2SOELogger *self = g_object_new(GSA2_TYPE_SOE_LOGGER, "level", SOUP_LOGGER_LOG_BODY, "max-body-size", -1, NULL);
    return self;
}
