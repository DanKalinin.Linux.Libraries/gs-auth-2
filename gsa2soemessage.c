//
// Created by root on 03.06.2020.
//

#include "gsa2soemessage.h"

void gsa2_soe_message_finalize(GObject *self);
gboolean gsa2_soe_message_set_error(GSA2SOEMessage *self, GError **error);

G_DEFINE_TYPE(GSA2SOEMessage, gsa2_soe_message, SOE_TYPE_MESSAGE)

static void gsa2_soe_message_class_init(GSA2SOEMessageClass *class) {
    G_OBJECT_CLASS(class)->finalize = gsa2_soe_message_finalize;
}

static void gsa2_soe_message_init(GSA2SOEMessage *self) {

}

void gsa2_soe_message_finalize(GObject *self) {
    G_OBJECT_CLASS(gsa2_soe_message_parent_class)->finalize(self);
}

gboolean gsa2_soe_message_set_error(GSA2SOEMessage *self, GError **error) {
    if (SOUP_MESSAGE(self)->response_body->length > 0) {
        gchar *content_type = (gchar *)soup_message_headers_get_content_type(SOUP_MESSAGE(self)->response_headers, NULL);

        if (g_strcmp0(content_type, JSE_MIME_TYPE) == 0) {
            g_autoptr(GSA2JSEParser) parser = gsa2_jse_parser_new();
            if (!json_parser_load_from_data(JSON_PARSER(parser), SOUP_MESSAGE(self)->response_body->data, SOUP_MESSAGE(self)->response_body->length, error)) return FALSE;
            JsonNode *node = json_parser_get_root(JSON_PARSER(parser));

            JsonObject *object = json_node_get_object(node);
            GError *ret_error = gsa2_jse_reader_error(object);
            g_propagate_error(error, ret_error);
        } else {
            soe_http_set_error(error, SOUP_MESSAGE(self)->status_code);
        }
    } else {
        soe_http_set_error(error, SOUP_MESSAGE(self)->status_code);
    }

    return TRUE;
}

GSA2SOEMessage *gsa2_soe_message_identity_check(SoupURI *base, GSA2Identity *identity) {
    g_autoptr(JsonObject) object = gsa2_jse_builder_identity_check(identity);
    g_autoptr(JsonNode) node = json_node_new(JSON_NODE_OBJECT);
    json_node_set_object(node, object);

    g_autoptr(GSA2JSEGenerator) generator = gsa2_jse_generator_new(node);
    gsize length = 0;
    gchar *body = json_generator_to_data(JSON_GENERATOR(generator), &length);

    g_autoptr(SoupURI) uri = soup_uri_new_with_base(base, "v1/account/check-identity");

    GSA2SOEMessage *self = g_object_new(GSA2_TYPE_SOE_MESSAGE, "method", SOUP_METHOD_POST, "uri", uri, NULL);
    soup_message_set_request(SOUP_MESSAGE(self), JSE_MIME_TYPE, SOUP_MEMORY_TAKE, body, length);
    return self;
}

gboolean gsa2_soe_message_identity_check_finish(GSA2SOEMessage *self, gboolean *available, GError **error) {
    if (SOUP_MESSAGE(self)->status_code == SOUP_STATUS_OK) {
        g_autoptr(GSA2JSEParser) parser = gsa2_jse_parser_new();
        if (!json_parser_load_from_data(JSON_PARSER(parser), SOUP_MESSAGE(self)->response_body->data, SOUP_MESSAGE(self)->response_body->length, error)) return FALSE;
        JsonNode *node = json_parser_get_root(JSON_PARSER(parser));

        JsonObject *object = json_node_get_object(node);
        gboolean ret_available = gsa2_jse_reader_identity_check(object);
        GE_SET_VALUE(available, ret_available);
    } else {
        (void)gsa2_soe_message_set_error(self, error);
        return FALSE;
    }

    return TRUE;
}

GSA2SOEMessage *gsa2_soe_message_code_send(SoupURI *base, GSA2Identity *identity) {
    g_autoptr(JsonObject) object = gsa2_jse_builder_code_send(identity);
    g_autoptr(JsonNode) node = json_node_new(JSON_NODE_OBJECT);
    json_node_set_object(node, object);

    g_autoptr(GSA2JSEGenerator) generator = gsa2_jse_generator_new(node);
    gsize length = 0;
    gchar *body = json_generator_to_data(JSON_GENERATOR(generator), &length);

    g_autoptr(SoupURI) uri = soup_uri_new_with_base(base, "v1/account/activate/request-code");

    GSA2SOEMessage *self = g_object_new(GSA2_TYPE_SOE_MESSAGE, "method", SOUP_METHOD_POST, "uri", uri, NULL);
    soup_message_set_request(SOUP_MESSAGE(self), JSE_MIME_TYPE, SOUP_MEMORY_TAKE, body, length);
    return self;
}

GDateTime *gsa2_soe_message_code_send_finish(GSA2SOEMessage *self, GError **error) {
    if (SOUP_MESSAGE(self)->status_code == SOUP_STATUS_OK) {
        g_autoptr(GSA2JSEParser) parser = gsa2_jse_parser_new();
        if (!json_parser_load_from_data(JSON_PARSER(parser), SOUP_MESSAGE(self)->response_body->data, SOUP_MESSAGE(self)->response_body->length, error)) return NULL;
        JsonNode *node = json_parser_get_root(JSON_PARSER(parser));

        JsonObject *object = json_node_get_object(node);
        GDateTime *ret = gsa2_jse_reader_code_send(object);
        return ret;
    } else {
        (void)gsa2_soe_message_set_error(self, error);
    }

    return NULL;
}

GSA2SOEMessage *gsa2_soe_message_account_create(SoupURI *base, GSA2Account *account, GList *identities) {
    g_autoptr(JsonObject) object = gsa2_jse_builder_account_create(account, identities);
    g_autoptr(JsonNode) node = json_node_new(JSON_NODE_OBJECT);
    json_node_set_object(node, object);

    g_autoptr(GSA2JSEGenerator) generator = gsa2_jse_generator_new(node);
    gsize length = 0;
    gchar *body = json_generator_to_data(JSON_GENERATOR(generator), &length);

    g_autoptr(SoupURI) uri = soup_uri_new_with_base(base, "v1/account");

    GSA2SOEMessage *self = g_object_new(GSA2_TYPE_SOE_MESSAGE, "method", SOUP_METHOD_POST, "uri", uri, NULL);
    soup_message_set_request(SOUP_MESSAGE(self), JSE_MIME_TYPE, SOUP_MEMORY_TAKE, body, length);
    return self;
}

GSA2Account *gsa2_soe_message_account_create_finish(GSA2SOEMessage *self, GSA2SQLEConnection *connection, GDateTime **expiration, GError **error) {
    if (SOUP_MESSAGE(self)->status_code == SOUP_STATUS_OK) {
        g_autoptr(GSA2JSEParser) parser = gsa2_jse_parser_new();
        if (!json_parser_load_from_data(JSON_PARSER(parser), SOUP_MESSAGE(self)->response_body->data, SOUP_MESSAGE(self)->response_body->length, error)) return NULL;
        JsonNode *node = json_parser_get_root(JSON_PARSER(parser));

        JsonObject *object = json_node_get_object(node);
        GSA2Account *ret = NULL;
        if ((ret = gsa2_jse_reader_account_create(object, connection, expiration, error)) == NULL) return NULL;
        return ret;
    } else {
        (void)gsa2_soe_message_set_error(self, error);
    }

    return NULL;
}

GSA2SOEMessage *gsa2_soe_message_account_get(SoupURI *base, gchar *access) {
    g_autoptr(SoupURI) uri = soup_uri_new_with_base(base, "v1/my/account");

    GSA2SOEMessage *self = g_object_new(GSA2_TYPE_SOE_MESSAGE, "method", SOUP_METHOD_GET, "uri", uri, NULL);
    soe_message_headers_set_authorization_bearer_token(SOUP_MESSAGE(self)->request_headers, access);
    return self;
}

GSA2Account *gsa2_soe_message_account_get_finish(GSA2SOEMessage *self, GSA2SQLEConnection *connection, GError **error) {
    if (SOUP_MESSAGE(self)->status_code == SOUP_STATUS_OK) {
        g_autoptr(GSA2JSEParser) parser = gsa2_jse_parser_new();
        if (!json_parser_load_from_data(JSON_PARSER(parser), SOUP_MESSAGE(self)->response_body->data, SOUP_MESSAGE(self)->response_body->length, error)) return NULL;
        JsonNode *node = json_parser_get_root(JSON_PARSER(parser));

        JsonObject *object = json_node_get_object(node);
        GSA2Account *ret = NULL;
        if ((ret = gsa2_jse_reader_account_get(object, connection, error)) == NULL) return NULL;
        return ret;
    } else {
        (void)gsa2_soe_message_set_error(self, error);
    }

    return NULL;
}

GSA2SOEMessage *gsa2_soe_message_password_update(SoupURI *base, gchar *password, gchar *access) {
    g_autoptr(JsonObject) object = gsa2_jse_builder_password_update(password);
    g_autoptr(JsonNode) node = json_node_new(JSON_NODE_OBJECT);
    json_node_set_object(node, object);

    g_autoptr(GSA2JSEGenerator) generator = gsa2_jse_generator_new(node);
    gsize length = 0;
    gchar *body = json_generator_to_data(JSON_GENERATOR(generator), &length);

    g_autoptr(SoupURI) uri = soup_uri_new_with_base(base, "v1/my/account/password");

    GSA2SOEMessage *self = g_object_new(GSA2_TYPE_SOE_MESSAGE, "method", SOUP_METHOD_PUT, "uri", uri, NULL);
    soe_message_headers_set_authorization_bearer_token(SOUP_MESSAGE(self)->request_headers, access);
    soup_message_set_request(SOUP_MESSAGE(self), JSE_MIME_TYPE, SOUP_MEMORY_TAKE, body, length);
    return self;
}

gboolean gsa2_soe_message_password_update_finish(GSA2SOEMessage *self, GError **error) {
    if (SOUP_MESSAGE(self)->status_code == SOUP_STATUS_OK) {
        g_autoptr(GSA2JSEParser) parser = gsa2_jse_parser_new();
        if (!json_parser_load_from_data(JSON_PARSER(parser), SOUP_MESSAGE(self)->response_body->data, SOUP_MESSAGE(self)->response_body->length, error)) return FALSE;
        JsonNode *node = json_parser_get_root(JSON_PARSER(parser));

        JsonObject *object = json_node_get_object(node);
        gsa2_jse_reader_password_update(object);
    } else {
        (void)gsa2_soe_message_set_error(self, error);
        return FALSE;
    }

    return TRUE;
}

GSA2SOEMessage *gsa2_soe_message_domain_create(SoupURI *base, GSA2Domain *domain, GList *controllers_full, gchar *access) {
    g_autoptr(JsonObject) object = gsa2_jse_builder_domain_create(domain, controllers_full);
    g_autoptr(JsonNode) node = json_node_new(JSON_NODE_OBJECT);
    json_node_set_object(node, object);

    g_autoptr(GSA2JSEGenerator) generator = gsa2_jse_generator_new(node);
    gsize length = 0;
    gchar *body = json_generator_to_data(JSON_GENERATOR(generator), &length);

    g_autoptr(SoupURI) uri = soup_uri_new_with_base(base, "v1/my/domain");

    GSA2SOEMessage *self = g_object_new(GSA2_TYPE_SOE_MESSAGE, "method", SOUP_METHOD_POST, "uri", uri, NULL);
    soe_message_headers_set_authorization_bearer_token(SOUP_MESSAGE(self)->request_headers, access);
    soup_message_set_request(SOUP_MESSAGE(self), JSE_MIME_TYPE, SOUP_MEMORY_TAKE, body, length);
    return self;
}

GSA2DomainFull *gsa2_soe_message_domain_create_finish(GSA2SOEMessage *self, GSA2SQLEConnection *connection, GError **error) {
    if (SOUP_MESSAGE(self)->status_code == SOUP_STATUS_OK) {
        g_autofree gchar *access = soe_message_headers_get_authorization_bearer_token(SOUP_MESSAGE(self)->request_headers);
        g_autoptr(JWTToken) token = NULL;
        if ((token = jwt_token_decode(access, error)) == NULL) return NULL;
        gchar *account_id = (gchar *)json_object_get_string_member(token->payload, "id");

        g_autoptr(GSA2JSEParser) parser = gsa2_jse_parser_new();
        if (!json_parser_load_from_data(JSON_PARSER(parser), SOUP_MESSAGE(self)->response_body->data, SOUP_MESSAGE(self)->response_body->length, error)) return NULL;
        JsonNode *node = json_parser_get_root(JSON_PARSER(parser));

        JsonObject *object = json_node_get_object(node);
        GSA2DomainFull *ret = NULL;
        if ((ret = gsa2_jse_reader_domain_create(object, connection, account_id, error)) == NULL) return NULL;
        return ret;
    } else {
        (void)gsa2_soe_message_set_error(self, error);
    }

    return NULL;
}

GSA2SOEMessage *gsa2_soe_message_domain_rename(SoupURI *base, gchar *domain_id, gchar *name, gchar *access) {
    g_autoptr(JsonObject) object = gsa2_jse_builder_domain_rename(name);
    g_autoptr(JsonNode) node = json_node_new(JSON_NODE_OBJECT);
    json_node_set_object(node, object);
    
    g_autoptr(GSA2JSEGenerator) generator = gsa2_jse_generator_new(node);
    gsize length = 0;
    gchar *body = json_generator_to_data(JSON_GENERATOR(generator), &length);
    
    g_autofree gchar *path = g_strdup_printf("v1/my/domain/%s", domain_id);
    g_autoptr(SoupURI) uri = soup_uri_new_with_base(base, path);
    
    GSA2SOEMessage *self = g_object_new(GSA2_TYPE_SOE_MESSAGE, "method", SOUP_METHOD_PUT, "uri", uri, NULL);
    soe_message_headers_set_authorization_bearer_token(SOUP_MESSAGE(self)->request_headers, access);
    soup_message_set_request(SOUP_MESSAGE(self), JSE_MIME_TYPE, SOUP_MEMORY_TAKE, body, length);
    return self;
}

gboolean gsa2_soe_message_domain_rename_finish(GSA2SOEMessage *self, GSA2SQLEConnection *connection, GError **error) {
    if (SOUP_MESSAGE(self)->status_code == SOUP_STATUS_OK) {
        SoupURI *uri = soup_message_get_uri(SOUP_MESSAGE(self));
        g_auto(GStrv) components = g_strsplit(uri->path, "/", -1);
        gchar *domain_id = components[4];

        g_autoptr(GSA2JSEParser) parser = gsa2_jse_parser_new();
        if (!json_parser_load_from_data(JSON_PARSER(parser), SOUP_MESSAGE(self)->request_body->data, SOUP_MESSAGE(self)->request_body->length, error)) return FALSE;
        JsonNode *node = json_parser_get_root(JSON_PARSER(parser));

        JsonObject *object = json_node_get_object(node);
        if (!gsa2_jse_reader_domain_rename(object, connection, domain_id, error)) return FALSE;
    } else {
        (void)gsa2_soe_message_set_error(self, error);
        return FALSE;
    }

    return TRUE;
}

GSA2SOEMessage *gsa2_soe_message_domain_delete(SoupURI *base, gchar *domain_id, gchar *access) {
    g_autofree gchar *path = g_strdup_printf("v1/my/domain/%s", domain_id);
    g_autoptr(SoupURI) uri = soup_uri_new_with_base(base, path);

    GSA2SOEMessage *self = g_object_new(GSA2_TYPE_SOE_MESSAGE, "method", SOUP_METHOD_DELETE, "uri", uri, NULL);
    soe_message_headers_set_authorization_bearer_token(SOUP_MESSAGE(self)->request_headers, access);
    return self;
}

gboolean gsa2_soe_message_domain_delete_finish(GSA2SOEMessage *self, GSA2SQLEConnection *connection, GError **error) {
    if (SOUP_MESSAGE(self)->status_code == SOUP_STATUS_OK) {
        g_autofree gchar *access = soe_message_headers_get_authorization_bearer_token(SOUP_MESSAGE(self)->request_headers);
        g_autoptr(JWTToken) token = NULL;
        if ((token = jwt_token_decode(access, error)) == NULL) return FALSE;
        gchar *account_id = (gchar *)json_object_get_string_member(token->payload, "id");

        SoupURI *uri = soup_message_get_uri(SOUP_MESSAGE(self));
        g_auto(GStrv) components = g_strsplit(uri->path, "/", -1);
        gchar *domain_id = components[4];

        g_autoptr(GSA2JSEParser) parser = gsa2_jse_parser_new();
        if (!json_parser_load_from_data(JSON_PARSER(parser), SOUP_MESSAGE(self)->response_body->data, SOUP_MESSAGE(self)->response_body->length, error)) return FALSE;
        JsonNode *node = json_parser_get_root(JSON_PARSER(parser));

        JsonObject *object = json_node_get_object(node);
        if (!gsa2_jse_reader_domain_delete(object, connection, account_id, domain_id, error)) return FALSE;
    } else {
        (void)gsa2_soe_message_set_error(self, error);
        return FALSE;
    }

    return TRUE;
}

GSA2SOEMessage *gsa2_soe_message_domain_exit(SoupURI *base, gchar *domain_id, gchar *access) {
    g_autofree gchar *path = g_strdup_printf("v1/my/account/domain/%s", domain_id);
    g_autoptr(SoupURI) uri = soup_uri_new_with_base(base, path);

    GSA2SOEMessage *self = g_object_new(GSA2_TYPE_SOE_MESSAGE, "method", SOUP_METHOD_DELETE, "uri", uri, NULL);
    soe_message_headers_set_authorization_bearer_token(SOUP_MESSAGE(self)->request_headers, access);
    return self;
}

gboolean gsa2_soe_message_domain_exit_finish(GSA2SOEMessage *self, GSA2SQLEConnection *connection, GError **error) {
    if (SOUP_MESSAGE(self)->status_code == SOUP_STATUS_OK) {
        g_autofree gchar *access = soe_message_headers_get_authorization_bearer_token(SOUP_MESSAGE(self)->request_headers);
        g_autoptr(JWTToken) token = NULL;
        if ((token = jwt_token_decode(access, error)) == NULL) return FALSE;
        gchar *account_id = (gchar *)json_object_get_string_member(token->payload, "id");

        SoupURI *uri = soup_message_get_uri(SOUP_MESSAGE(self));
        g_auto(GStrv) components = g_strsplit(uri->path, "/", -1);
        gchar *domain_id = components[5];

        g_autoptr(GSA2JSEParser) parser = gsa2_jse_parser_new();
        if (!json_parser_load_from_data(JSON_PARSER(parser), SOUP_MESSAGE(self)->response_body->data, SOUP_MESSAGE(self)->response_body->length, error)) return FALSE;
        JsonNode *node = json_parser_get_root(JSON_PARSER(parser));

        JsonObject *object = json_node_get_object(node);
        if (!gsa2_jse_reader_domain_exit(object, connection, account_id, domain_id, error)) return FALSE;
    } else {
        (void)gsa2_soe_message_set_error(self, error);
        return FALSE;
    }

    return TRUE;
}

GSA2SOEMessage *gsa2_soe_message_guest_delete(SoupURI *base, gchar *guest_id, gchar *domain_id, gchar *access) {
    g_autofree gchar *path = g_strdup_printf("v1/my/domain/%s/account/%s", domain_id, guest_id);
    g_autoptr(SoupURI) uri = soup_uri_new_with_base(base, path);
    
    GSA2SOEMessage *self = g_object_new(GSA2_TYPE_SOE_MESSAGE, "method", SOUP_METHOD_DELETE, "uri", uri, NULL);
    soe_message_headers_set_authorization_bearer_token(SOUP_MESSAGE(self)->request_headers, access);
    return self;
}

GSA2DomainFull *gsa2_soe_message_guest_delete_finish(GSA2SOEMessage *self, GSA2SQLEConnection *connection, GError **error) {
    if (SOUP_MESSAGE(self)->status_code == SOUP_STATUS_OK) {
        g_autofree gchar *access = soe_message_headers_get_authorization_bearer_token(SOUP_MESSAGE(self)->request_headers);
        g_autoptr(JWTToken) token = NULL;
        if ((token = jwt_token_decode(access, error)) == NULL) return NULL;
        gchar *account_id = (gchar *)json_object_get_string_member(token->payload, "id");

        g_autoptr(GSA2JSEParser) parser = gsa2_jse_parser_new();
        if (!json_parser_load_from_data(JSON_PARSER(parser), SOUP_MESSAGE(self)->response_body->data, SOUP_MESSAGE(self)->response_body->length, error)) return NULL;
        JsonNode *node = json_parser_get_root(JSON_PARSER(parser));

        JsonObject *object = json_node_get_object(node);
        GSA2DomainFull *ret = NULL;
        if ((ret = gsa2_jse_reader_guest_delete(object, connection, account_id, error)) == NULL) return NULL;
        return ret;
    } else {
        (void)gsa2_soe_message_set_error(self, error);
    }

    return NULL;
}

GSA2SOEMessage *gsa2_soe_message_invitation_create(SoupURI *base, GSA2Invitation *invitation, gchar *domain_id, gchar *access) {
    g_autoptr(JsonObject) object = gsa2_jse_builder_invitation_create(invitation);
    g_autoptr(JsonNode) node = json_node_new(JSON_NODE_OBJECT);
    json_node_set_object(node, object);

    g_autoptr(GSA2JSEGenerator) generator = gsa2_jse_generator_new(node);
    gsize length = 0;
    gchar *body = json_generator_to_data(JSON_GENERATOR(generator), &length);

    g_autofree gchar *path = g_strdup_printf("v1/my/domain/%s/invite/request-code", domain_id);
    g_autoptr(SoupURI) uri = soup_uri_new_with_base(base, path);

    GSA2SOEMessage *self = g_object_new(GSA2_TYPE_SOE_MESSAGE, "method", SOUP_METHOD_POST, "uri", uri, NULL);
    soe_message_headers_set_authorization_bearer_token(SOUP_MESSAGE(self)->request_headers, access);
    soup_message_set_request(SOUP_MESSAGE(self), JSE_MIME_TYPE, SOUP_MEMORY_TAKE, body, length);
    return self;
}

GSA2Invitation *gsa2_soe_message_invitation_create_finish(GSA2SOEMessage *self, GSA2SQLEConnection *connection, GError **error) {
    if (SOUP_MESSAGE(self)->status_code == SOUP_STATUS_OK) {
        SoupURI *uri = soup_message_get_uri(SOUP_MESSAGE(self));
        g_auto(GStrv) components = g_strsplit(uri->path, "/", -1);
        gchar *domain_id = components[4];

        g_autoptr(GSA2JSEParser) parser = gsa2_jse_parser_new();
        if (!json_parser_load_from_data(JSON_PARSER(parser), SOUP_MESSAGE(self)->response_body->data, SOUP_MESSAGE(self)->response_body->length, error)) return NULL;
        JsonNode *node = json_parser_get_root(JSON_PARSER(parser));

        JsonObject *object = json_node_get_object(node);
        GSA2Invitation *ret = NULL;
        if ((ret = gsa2_jse_reader_invitation_create(object, connection, domain_id, error)) == NULL) return NULL;
        return ret;
    } else {
        (void)gsa2_soe_message_set_error(self, error);
    }

    return NULL;
}

GSA2SOEMessage *gsa2_soe_message_invitation_revoke(SoupURI *base, gchar *invitation_id, gchar *domain_id, gchar *access) {
    g_autofree gchar *path = g_strdup_printf("v1/my/domain/%s/invite/%s", domain_id, invitation_id);
    g_autoptr(SoupURI) uri = soup_uri_new_with_base(base, path);

    GSA2SOEMessage *self = g_object_new(GSA2_TYPE_SOE_MESSAGE, "method", SOUP_METHOD_DELETE, "uri", uri, NULL);
    soe_message_headers_set_authorization_bearer_token(SOUP_MESSAGE(self)->request_headers, access);
    return self;
}

gboolean gsa2_soe_message_invitation_revoke_finish(GSA2SOEMessage *self, GSA2SQLEConnection *connection, GError **error) {
    if (SOUP_MESSAGE(self)->status_code == SOUP_STATUS_OK) {
        SoupURI *uri = soup_message_get_uri(SOUP_MESSAGE(self));
        g_auto(GStrv) components = g_strsplit(uri->path, "/", -1);
        gchar *domain_id = components[4];
        gchar *invitation_id = components[6];

        g_autoptr(GSA2JSEParser) parser = gsa2_jse_parser_new();
        if (!json_parser_load_from_data(JSON_PARSER(parser), SOUP_MESSAGE(self)->response_body->data, SOUP_MESSAGE(self)->response_body->length, error)) return FALSE;
        JsonNode *node = json_parser_get_root(JSON_PARSER(parser));

        JsonObject *object = json_node_get_object(node);
        if (!gsa2_jse_reader_invitation_revoke(object, connection, domain_id, invitation_id, error)) return FALSE;
    } else {
        (void)gsa2_soe_message_set_error(self, error);
        return FALSE;
    }

    return TRUE;
}

GSA2SOEMessage *gsa2_soe_message_invitation_accept(SoupURI *base, gchar *invitation_id, gchar *access) {
    g_autoptr(JsonObject) object = gsa2_jse_builder_invitation_accept(invitation_id);
    g_autoptr(JsonNode) node = json_node_new(JSON_NODE_OBJECT);
    json_node_set_object(node, object);

    g_autoptr(GSA2JSEGenerator) generator = gsa2_jse_generator_new(node);
    gsize length = 0;
    gchar *body = json_generator_to_data(JSON_GENERATOR(generator), &length);

    g_autoptr(SoupURI) uri = soup_uri_new_with_base(base, "v1/my/account/invite");

    GSA2SOEMessage *self = g_object_new(GSA2_TYPE_SOE_MESSAGE, "method", SOUP_METHOD_POST, "uri", uri, NULL);
    soe_message_headers_set_authorization_bearer_token(SOUP_MESSAGE(self)->request_headers, access);
    soup_message_set_request(SOUP_MESSAGE(self), JSE_MIME_TYPE, SOUP_MEMORY_TAKE, body, length);
    return self;
}

GSA2DomainFull *gsa2_soe_message_invitation_accept_finish(GSA2SOEMessage *self, GSA2SQLEConnection *connection, GError **error) {
    if (SOUP_MESSAGE(self)->status_code == SOUP_STATUS_OK) {
        g_autofree gchar *access = soe_message_headers_get_authorization_bearer_token(SOUP_MESSAGE(self)->request_headers);
        g_autoptr(JWTToken) token = NULL;
        if ((token = jwt_token_decode(access, error)) == NULL) return NULL;
        gchar *account_id = (gchar *)json_object_get_string_member(token->payload, "id");

        g_autoptr(GSA2JSEParser) parser = gsa2_jse_parser_new();
        if (!json_parser_load_from_data(JSON_PARSER(parser), SOUP_MESSAGE(self)->response_body->data, SOUP_MESSAGE(self)->response_body->length, error)) return NULL;
        JsonNode *node = json_parser_get_root(JSON_PARSER(parser));

        JsonObject *object = json_node_get_object(node);
        GSA2DomainFull *ret = NULL;
        if ((ret = gsa2_jse_reader_invitation_accept(object, connection, account_id, error)) == NULL) return NULL;
        return ret;
    } else {
        (void)gsa2_soe_message_set_error(self, error);
    }

    return NULL;
}

GSA2SOEMessage *gsa2_soe_message_code_get(SoupURI *base, gchar *domain_id, gchar *access) {
    g_autofree gchar *path = g_strdup_printf("v1/my/domain/%s/verify/request-code", domain_id);
    g_autoptr(SoupURI) uri = soup_uri_new_with_base(base, path);

    GSA2SOEMessage *self = g_object_new(GSA2_TYPE_SOE_MESSAGE, "method", SOUP_METHOD_POST, "uri", uri, NULL);
    soe_message_headers_set_authorization_bearer_token(SOUP_MESSAGE(self)->request_headers, access);
    return self;
}

gchar *gsa2_soe_message_code_get_finish(GSA2SOEMessage *self, GError **error) {
    if (SOUP_MESSAGE(self)->status_code == SOUP_STATUS_OK) {
        g_autoptr(GSA2JSEParser) parser = gsa2_jse_parser_new();
        if (!json_parser_load_from_data(JSON_PARSER(parser), SOUP_MESSAGE(self)->response_body->data, SOUP_MESSAGE(self)->response_body->length, error)) return NULL;
        JsonNode *node = json_parser_get_root(JSON_PARSER(parser));

        JsonObject *object = json_node_get_object(node);
        gchar *ret = gsa2_jse_reader_code_get(object);
        return ret;
    } else {
        (void)gsa2_soe_message_set_error(self, error);
    }

    return NULL;
}

GSA2SOEMessage *gsa2_soe_message_controllers_add(SoupURI *base, GList *controllers_full, gchar *domain_id, gchar *access) {
    g_autoptr(JsonObject) object = gsa2_jse_builder_controllers_add(controllers_full);
    g_autoptr(JsonNode) node = json_node_new(JSON_NODE_OBJECT);
    json_node_set_object(node, object);

    g_autoptr(GSA2JSEGenerator) generator = gsa2_jse_generator_new(node);
    gsize length = 0;
    gchar *body = json_generator_to_data(JSON_GENERATOR(generator), &length);

    g_autofree gchar *path = g_strdup_printf("v1/my/domain/%s/controller", domain_id);
    g_autoptr(SoupURI) uri = soup_uri_new_with_base(base, path);

    GSA2SOEMessage *self = g_object_new(GSA2_TYPE_SOE_MESSAGE, "method", SOUP_METHOD_POST, "uri", uri, NULL);
    soe_message_headers_set_authorization_bearer_token(SOUP_MESSAGE(self)->request_headers, access);
    soup_message_set_request(SOUP_MESSAGE(self), JSE_MIME_TYPE, SOUP_MEMORY_TAKE, body, length);
    return self;
}

gboolean gsa2_soe_message_controllers_add_finish(GSA2SOEMessage *self, GSA2SQLEConnection *connection, GList **controllers_full, GError **error) {
    if (SOUP_MESSAGE(self)->status_code == SOUP_STATUS_OK) {
        SoupURI *uri = soup_message_get_uri(SOUP_MESSAGE(self));
        g_auto(GStrv) components = g_strsplit(uri->path, "/", -1);
        gchar *domain_id = components[4];

        g_autoptr(GSA2JSEParser) parser = gsa2_jse_parser_new();
        if (!json_parser_load_from_data(JSON_PARSER(parser), SOUP_MESSAGE(self)->response_body->data, SOUP_MESSAGE(self)->response_body->length, error)) return FALSE;
        JsonNode *node = json_parser_get_root(JSON_PARSER(parser));

        JsonObject *object = json_node_get_object(node);
        if (!gsa2_jse_reader_controllers_add(object, connection, domain_id, controllers_full, error)) return FALSE;
    } else {
        (void)gsa2_soe_message_set_error(self, error);
        return FALSE;
    }

    return TRUE;
}

GSA2SOEMessage *gsa2_soe_message_controller_remove(SoupURI *base, gchar *controller_id, gchar *domain_id, gchar *access) {
    g_autofree gchar *path = g_strdup_printf("v1/my/domain/%s/controller/%s", domain_id, controller_id);
    g_autoptr(SoupURI) uri = soup_uri_new_with_base(base, path);

    GSA2SOEMessage *self = g_object_new(GSA2_TYPE_SOE_MESSAGE, "method", SOUP_METHOD_DELETE, "uri", uri, NULL);
    soe_message_headers_set_authorization_bearer_token(SOUP_MESSAGE(self)->request_headers, access);
    return self;
}

gboolean gsa2_soe_message_controller_remove_finish(GSA2SOEMessage *self, GSA2SQLEConnection *connection, GError **error) {
    if (SOUP_MESSAGE(self)->status_code == SOUP_STATUS_OK) {
        SoupURI *uri = soup_message_get_uri(SOUP_MESSAGE(self));
        g_auto(GStrv) components = g_strsplit(uri->path, "/", -1);
        gchar *domain_id = components[4];
        gchar *controller_id = components[6];

        g_autoptr(GSA2JSEParser) parser = gsa2_jse_parser_new();
        if (!json_parser_load_from_data(JSON_PARSER(parser), SOUP_MESSAGE(self)->response_body->data, SOUP_MESSAGE(self)->response_body->length, error)) return FALSE;
        JsonNode *node = json_parser_get_root(JSON_PARSER(parser));

        JsonObject *object = json_node_get_object(node);
        if (!gsa2_jse_reader_controller_remove(object, connection, domain_id, controller_id, error)) return FALSE;
    } else {
        (void)gsa2_soe_message_set_error(self, error);
        return FALSE;
    }

    return TRUE;
}

GSA2SOEMessage *gsa2_soe_message_controller_create(SoupURI *base, GSA2Controller *controller) {
    g_autoptr(JsonObject) object = gsa2_jse_builder_controller_create(controller);
    g_autoptr(JsonNode) node = json_node_new(JSON_NODE_OBJECT);
    json_node_set_object(node, object);

    g_autoptr(GSA2JSEGenerator) generator = gsa2_jse_generator_new(node);
    gsize length = 0;
    gchar *body = json_generator_to_data(JSON_GENERATOR(generator), &length);

    g_autoptr(SoupURI) uri = soup_uri_new_with_base(base, "v1/controller");

    GSA2SOEMessage *self = g_object_new(GSA2_TYPE_SOE_MESSAGE, "method", SOUP_METHOD_POST, "uri", uri, NULL);
    soup_message_set_request(SOUP_MESSAGE(self), JSE_MIME_TYPE, SOUP_MEMORY_TAKE, body, length);
    return self;
}

GSA2Controller *gsa2_soe_message_controller_create_finish(GSA2SOEMessage *self, GSA2SQLEConnection *connection, GError **error) {
    if (SOUP_MESSAGE(self)->status_code == SOUP_STATUS_OK) {
        g_autoptr(GSA2JSEParser) parser = gsa2_jse_parser_new();
        if (!json_parser_load_from_data(JSON_PARSER(parser), SOUP_MESSAGE(self)->response_body->data, SOUP_MESSAGE(self)->response_body->length, error)) return NULL;
        JsonNode *node = json_parser_get_root(JSON_PARSER(parser));

        JsonObject *object = json_node_get_object(node);
        GSA2Controller *ret = NULL;
        if ((ret = gsa2_jse_reader_controller_create(object, connection, error)) == NULL) return NULL;
        return ret;
    } else {
        (void)gsa2_soe_message_set_error(self, error);
    }

    return NULL;
}

GSA2SOEMessage *gsa2_soe_message_controller_verify(SoupURI *base, gchar *code, gchar *access) {
    g_autoptr(JsonObject) object = gsa2_jse_builder_controller_verify(code);
    g_autoptr(JsonNode) node = json_node_new(JSON_NODE_OBJECT);
    json_node_set_object(node, object);

    g_autoptr(GSA2JSEGenerator) generator = gsa2_jse_generator_new(node);
    gsize length = 0;
    gchar *body = json_generator_to_data(JSON_GENERATOR(generator), &length);

    g_autoptr(SoupURI) uri = soup_uri_new_with_base(base, "v1/my/controller/verify");

    GSA2SOEMessage *self = g_object_new(GSA2_TYPE_SOE_MESSAGE, "method", SOUP_METHOD_POST, "uri", uri, NULL);
    soe_message_headers_set_authorization_bearer_token(SOUP_MESSAGE(self)->request_headers, access);
    soup_message_set_request(SOUP_MESSAGE(self), JSE_MIME_TYPE, SOUP_MEMORY_TAKE, body, length);
    return self;
}

gboolean gsa2_soe_message_controller_verify_finish(GSA2SOEMessage *self, GSA2SQLEConnection *connection, GError **error) {
    if (SOUP_MESSAGE(self)->status_code == SOUP_STATUS_OK) {
        g_autofree gchar *access = soe_message_headers_get_authorization_bearer_token(SOUP_MESSAGE(self)->request_headers);
        g_autoptr(JWTToken) token = NULL;
        if ((token = jwt_token_decode(access, error)) == NULL) return FALSE;
        gchar *controller_id = (gchar *)json_object_get_string_member(token->payload, "id");
        
        g_autoptr(GSA2JSEParser) parser = gsa2_jse_parser_new();
        if (!json_parser_load_from_data(JSON_PARSER(parser), SOUP_MESSAGE(self)->response_body->data, SOUP_MESSAGE(self)->response_body->length, error)) return FALSE;
        JsonNode *node = json_parser_get_root(JSON_PARSER(parser));

        JsonObject *object = json_node_get_object(node);
        if (!gsa2_jse_reader_controller_verify(object, connection, controller_id, error)) return FALSE;
    } else {
        (void)gsa2_soe_message_set_error(self, error);
        return FALSE;
    }

    return TRUE;
}

GSA2SOEMessage *gsa2_soe_message_token_issue(SoupURI *base, GSA2Identity *identity, GSA2Credential *credential) {
    g_autoptr(JsonObject) object = gsa2_jse_builder_token_issue(identity, credential);
    g_autoptr(JsonNode) node = json_node_new(JSON_NODE_OBJECT);
    json_node_set_object(node, object);

    g_autoptr(GSA2JSEGenerator) generator = gsa2_jse_generator_new(node);
    gsize length = 0;
    gchar *body = json_generator_to_data(JSON_GENERATOR(generator), &length);

    g_autoptr(SoupURI) uri = soup_uri_new_with_base(base, "v1/token/issue");

    GSA2SOEMessage *self = g_object_new(GSA2_TYPE_SOE_MESSAGE, "method", SOUP_METHOD_POST, "uri", uri, NULL);
    soup_message_set_request(SOUP_MESSAGE(self), JSE_MIME_TYPE, SOUP_MEMORY_TAKE, body, length);
    return self;
}

GSA2Token *gsa2_soe_message_token_issue_finish(GSA2SOEMessage *self, GSA2SQLEConnection *connection, GError **error) {
    if (SOUP_MESSAGE(self)->status_code == SOUP_STATUS_OK) {
        g_autoptr(GSA2JSEParser) parser = gsa2_jse_parser_new();
        if (!json_parser_load_from_data(JSON_PARSER(parser), SOUP_MESSAGE(self)->response_body->data, SOUP_MESSAGE(self)->response_body->length, error)) return NULL;
        JsonNode *node = json_parser_get_root(JSON_PARSER(parser));

        JsonObject *object = json_node_get_object(node);
        GSA2Token *ret = NULL;
        if ((ret = gsa2_jse_reader_token_issue(object, connection, error)) == NULL) return NULL;
        return ret;
    } else {
        (void)gsa2_soe_message_set_error(self, error);
    }

    return NULL;
}

GSA2SOEMessage *gsa2_soe_message_token_refresh(SoupURI *base, gchar *refresh) {
    g_autoptr(JsonObject) object = gsa2_jse_builder_token_refresh(refresh);
    g_autoptr(JsonNode) node = json_node_new(JSON_NODE_OBJECT);
    json_node_set_object(node, object);

    g_autoptr(GSA2JSEGenerator) generator = gsa2_jse_generator_new(node);
    gsize length = 0;
    gchar *body = json_generator_to_data(JSON_GENERATOR(generator), &length);

    g_autoptr(SoupURI) uri = soup_uri_new_with_base(base, "v1/token/refresh");

    GSA2SOEMessage *self = g_object_new(GSA2_TYPE_SOE_MESSAGE, "method", SOUP_METHOD_POST, "uri", uri, NULL);
    soup_message_set_request(SOUP_MESSAGE(self), JSE_MIME_TYPE, SOUP_MEMORY_TAKE, body, length);
    return self;
}

GSA2Token *gsa2_soe_message_token_refresh_finish(GSA2SOEMessage *self, GSA2SQLEConnection *connection, GError **error) {
    if (SOUP_MESSAGE(self)->status_code == SOUP_STATUS_OK) {
        g_autoptr(GSA2JSEParser) parser = gsa2_jse_parser_new();
        if (!json_parser_load_from_data(JSON_PARSER(parser), SOUP_MESSAGE(self)->response_body->data, SOUP_MESSAGE(self)->response_body->length, error)) return NULL;
        JsonNode *node = json_parser_get_root(JSON_PARSER(parser));

        JsonObject *object = json_node_get_object(node);
        GSA2Token *ret = NULL;
        if ((ret = gsa2_jse_reader_token_refresh(object, connection, error)) == NULL) return NULL;
        return ret;
    } else {
        (void)gsa2_soe_message_set_error(self, error);
    }

    return NULL;
}

GSA2SOEMessage *gsa2_soe_message_token_revoke(SoupURI *base, gchar *access) {
    g_autoptr(JsonObject) object = gsa2_jse_builder_token_revoke(access);
    g_autoptr(JsonNode) node = json_node_new(JSON_NODE_OBJECT);
    json_node_set_object(node, object);

    g_autoptr(GSA2JSEGenerator) generator = gsa2_jse_generator_new(node);
    gsize length = 0;
    gchar *body = json_generator_to_data(JSON_GENERATOR(generator), &length);

    g_autoptr(SoupURI) uri = soup_uri_new_with_base(base, "v1/token/revoke");

    GSA2SOEMessage *self = g_object_new(GSA2_TYPE_SOE_MESSAGE, "method", SOUP_METHOD_POST, "uri", uri, NULL);
    soup_message_set_request(SOUP_MESSAGE(self), JSE_MIME_TYPE, SOUP_MEMORY_TAKE, body, length);
    return self;
}

gboolean gsa2_soe_message_token_revoke_finish(GSA2SOEMessage *self, GSA2SQLEConnection *connection, GError **error) {
    if (SOUP_MESSAGE(self)->status_code == SOUP_STATUS_OK) {
        g_autoptr(GSA2JSEParser) parser = gsa2_jse_parser_new();
        if (!json_parser_load_from_data(JSON_PARSER(parser), SOUP_MESSAGE(self)->request_body->data, SOUP_MESSAGE(self)->request_body->length, error)) return FALSE;
        JsonNode *node = json_parser_get_root(JSON_PARSER(parser));

        JsonObject *object = json_node_get_object(node);
        if (!gsa2_jse_reader_token_revoke(object, connection, error)) return FALSE;
    } else {
        (void)gsa2_soe_message_set_error(self, error);
        return FALSE;
    }

    return TRUE;
}

GSA2SOEMessage *gsa2_soe_message_camera_playlist(SoupURI *base, gchar *id, gchar *key, gchar *controller, gchar *access) {
    g_autoptr(JsonObject) object = gsa2_jse_builder_camera_playlist(key);
    g_autoptr(JsonNode) node = json_node_new(JSON_NODE_OBJECT);
    json_node_set_object(node, object);

    g_autoptr(GSA2JSEGenerator) generator = gsa2_jse_generator_new(node);
    gsize length = 0;
    gchar *body = json_generator_to_data(JSON_GENERATOR(generator), &length);

    g_autofree gchar *path = g_strdup_printf("cloud/v1/camera/%s/playlist", id);
    g_autoptr(SoupURI) uri = soup_uri_new_with_base(base, path);

    GSA2SOEMessage *self = g_object_new(GSA2_TYPE_SOE_MESSAGE, "method", SOUP_METHOD_POST, "uri", uri, NULL);
    soe_message_headers_set_authorization_bearer_token(SOUP_MESSAGE(self)->request_headers, access);
    gsa2_soe_message_headers_set_x_controller_id(SOUP_MESSAGE(self)->request_headers, controller);
    soup_message_set_request(SOUP_MESSAGE(self), JSE_MIME_TYPE, SOUP_MEMORY_TAKE, body, length);
    return self;
}

gchar *gsa2_soe_message_camera_playlist_finish(GSA2SOEMessage *self, GError **error) {
    if (SOUP_MESSAGE(self)->status_code == SOUP_STATUS_OK) {
        g_autoptr(GSA2JSEParser) parser = gsa2_jse_parser_new();
        if (!json_parser_load_from_data(JSON_PARSER(parser), SOUP_MESSAGE(self)->response_body->data, SOUP_MESSAGE(self)->response_body->length, error)) return NULL;
        JsonNode *node = json_parser_get_root(JSON_PARSER(parser));

        JsonObject *object = json_node_get_object(node);
        gchar *ret = gsa2_jse_reader_camera_playlist(object);
        return ret;
    } else {
        (void)gsa2_soe_message_set_error(self, error);
    }

    return NULL;
}

GSA2SOEMessage *gsa2_soe_message_camera_heartbeat(SoupURI *base, gchar *id, gchar *controller, gchar *access) {
    g_autofree gchar *path = g_strdup_printf("cloud/v1/camera/%s/heartbeat", id);
    g_autoptr(SoupURI) uri = soup_uri_new_with_base(base, path);

    GSA2SOEMessage *self = g_object_new(GSA2_TYPE_SOE_MESSAGE, "method", SOUP_METHOD_HEAD, "uri", uri, NULL);
    soe_message_headers_set_authorization_bearer_token(SOUP_MESSAGE(self)->request_headers, access);
    gsa2_soe_message_headers_set_x_controller_id(SOUP_MESSAGE(self)->request_headers, controller);
    return self;
}

gboolean gsa2_soe_message_camera_heartbeat_finish(GSA2SOEMessage *self, GError **error) {
    if (SOUP_MESSAGE(self)->status_code != SOUP_STATUS_OK) {
        (void)gsa2_soe_message_set_error(self, error);
        return FALSE;
    }

    return TRUE;
}
