//
// Created by root on 03.06.2020.
//

#ifndef LIBRARY_GS_AUTH_2_GSA2SOESESSION_H
#define LIBRARY_GS_AUTH_2_GSA2SOESESSION_H

#include "gsa2main.h"
#include "gsa2soelogger.h"
#include "gsa2soemessage.h"
#include "gsa2sqleconnection.h"

G_BEGIN_DECLS

#define GSA2_TYPE_SOE_SESSION gsa2_soe_session_get_type()

GE_DECLARE_DERIVABLE_TYPE(GSA2SOESession, gsa2_soe_session, GSA2, SOE_SESSION, SOESession)

struct _GSA2SOESessionClass {
    SOESessionClass super;
};

struct _GSA2SOESession {
    SOESession super;

    SoupURI *base;
    GSA2SQLEConnection *connection;
};

GE_STRUCTURE_FIELD(gsa2_soe_session, base, GSA2SOESession, SoupURI, soup_uri_copy, soup_uri_free, NULL)
GE_STRUCTURE_FIELD(gsa2_soe_session, connection, GSA2SOESession, GSA2SQLEConnection, g_object_ref, g_object_unref, NULL)

GSA2SOESession *gsa2_soe_session_new(SoupURI *base, GSA2SQLEConnection *connection);

gboolean gsa2_soe_session_identity_check_sync(GSA2SOESession *self, GSA2Identity *identity, gboolean *available, GError **error);
GDateTime *gsa2_soe_session_code_send_sync(GSA2SOESession *self, GSA2Identity *identity, GError **error);
GSA2Account *gsa2_soe_session_account_create_sync(GSA2SOESession *self, GSA2Account *account, GList *identities, GDateTime **expiration, GError **error);
GSA2Account *gsa2_soe_session_account_get_sync(GSA2SOESession *self, gchar *access, GError **error);
gboolean gsa2_soe_session_password_update_sync(GSA2SOESession *self, gchar *password, gchar *access, GError **error);
GSA2DomainFull *gsa2_soe_session_domain_create_sync(GSA2SOESession *self, GSA2Domain *domain, GList *controllers_full, gchar *access, GError **error);
gboolean gsa2_soe_session_domain_rename_sync(GSA2SOESession *self, gchar *domain_id, gchar *name, gchar *access, GError **error);
gboolean gsa2_soe_session_domain_delete_sync(GSA2SOESession *self, gchar *domain_id, gchar *access, GError **error);
gboolean gsa2_soe_session_domain_exit_sync(GSA2SOESession *self, gchar *domain_id, gchar *access, GError **error);
GSA2DomainFull *gsa2_soe_session_guest_delete_sync(GSA2SOESession *self, gchar *guest_id, gchar *domain_id, gchar *access, GError **error);
GSA2Invitation *gsa2_soe_session_invitation_create_sync(GSA2SOESession *self, GSA2Invitation *invitation, gchar *domain_id, gchar *access, GError **error);
gboolean gsa2_soe_session_invitation_revoke_sync(GSA2SOESession *self, gchar *invitation_id, gchar *domain_id, gchar *access, GError **error);
GSA2DomainFull *gsa2_soe_session_invitation_accept_sync(GSA2SOESession *self, gchar *invitation_id, gchar *access, GError **error);
gchar *gsa2_soe_session_code_get_sync(GSA2SOESession *self, gchar *domain_id, gchar *access, GError **error);
gboolean gsa2_soe_session_controllers_add_sync(GSA2SOESession *self, GList *controllers_full, gchar *domain_id, gchar *access, GList **out_controllers_full, GError **error);
gboolean gsa2_soe_session_controller_remove_sync(GSA2SOESession *self, gchar *controller_id, gchar *domain_id, gchar *access, GError **error);
GSA2Controller *gsa2_soe_session_controller_create_sync(GSA2SOESession *self, GSA2Controller *controller, GError **error);
gboolean gsa2_soe_session_controller_verify_sync(GSA2SOESession *self, gchar *code, gchar *access, GError **error);
GSA2Token *gsa2_soe_session_token_issue_sync(GSA2SOESession *self, GSA2Identity *identity, GSA2Credential *credential, GError **error);
GSA2Token *gsa2_soe_session_token_refresh_sync(GSA2SOESession *self, gchar *refresh, GError **error);
gboolean gsa2_soe_session_token_revoke_sync(GSA2SOESession *self, gchar *access, GError **error);
gchar *gsa2_soe_session_camera_playlist_sync(GSA2SOESession *self, gchar *id, gchar *key, gchar *controller, gchar *access, GError **error);
gboolean gsa2_soe_session_camera_heartbeat_sync(GSA2SOESession *self, gchar *id, gchar *controller, gchar *access, GError **error);

G_END_DECLS

#endif //LIBRARY_GS_AUTH_2_GSA2SOESESSION_H
