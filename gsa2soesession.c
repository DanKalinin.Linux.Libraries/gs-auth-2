//
// Created by root on 03.06.2020.
//

#include "gsa2soesession.h"

void gsa2_soe_session_finalize(GObject *self);
guint gsa2_soe_session_send_message(GSA2SOESession *self, GSA2SOEMessage *message);

G_DEFINE_TYPE(GSA2SOESession, gsa2_soe_session, SOE_TYPE_SESSION)

static void gsa2_soe_session_class_init(GSA2SOESessionClass *class) {
    G_OBJECT_CLASS(class)->finalize = gsa2_soe_session_finalize;
}

static void gsa2_soe_session_init(GSA2SOESession *self) {

}

void gsa2_soe_session_finalize(GObject *self) {
    gsa2_soe_session_set_base(GSA2_SOE_SESSION(self), NULL);
    gsa2_soe_session_set_connection(GSA2_SOE_SESSION(self), NULL);

    G_OBJECT_CLASS(gsa2_soe_session_parent_class)->finalize(self);
}

guint gsa2_soe_session_send_message(GSA2SOESession *self, GSA2SOEMessage *message) {
    guint ret = soup_session_send_message(SOUP_SESSION(self), SOUP_MESSAGE(message));
    
    if (ret == SOUP_STATUS_UNAUTHORIZED) {
        g_autofree gchar *access = NULL;
        if ((access = soe_message_headers_get_authorization_bearer_token(SOUP_MESSAGE(message)->request_headers)) == NULL) return ret;
        g_autolist(GSA2Token) tokens = NULL;
        if (gsa2_sqle_connection_token_select_by_access(self->connection, access, &tokens, NULL) != SQLITE_DONE) return ret;
        if (tokens == NULL) return ret;
        g_autoptr(GSA2Token) token = NULL;
        if ((token = gsa2_soe_session_token_refresh_sync(self, ((GSA2Token *)tokens->data)->refresh, NULL)) == NULL) return ret;
        soe_message_headers_set_authorization_bearer_token(SOUP_MESSAGE(message)->request_headers, token->access);
        ret = soup_session_send_message(SOUP_SESSION(self), SOUP_MESSAGE(message));
    }
    
    return ret;
}

GSA2SOESession *gsa2_soe_session_new(SoupURI *base, GSA2SQLEConnection *connection) {
    GSA2SOESession *self = g_object_new(GSA2_TYPE_SOE_SESSION, "accept-language-auto", TRUE, "timeout", 5, NULL);

    gsa2_soe_session_set_base(self, base);
    gsa2_soe_session_set_connection(self, connection);

    GSA2SOELogger *logger = gsa2_soe_logger_new();
    soup_session_add_feature(SOUP_SESSION(self), SOUP_SESSION_FEATURE(logger));

    return self;
}

gboolean gsa2_soe_session_identity_check_sync(GSA2SOESession *self, GSA2Identity *identity, gboolean *available, GError **error) {
    g_autoptr(GSA2SOEMessage) message = gsa2_soe_message_identity_check(self->base, identity);
    (void)gsa2_soe_session_send_message(self, message);
    gboolean ret = gsa2_soe_message_identity_check_finish(message, available, error);
    return ret;
}

GDateTime *gsa2_soe_session_code_send_sync(GSA2SOESession *self, GSA2Identity *identity, GError **error) {
    g_autoptr(GSA2SOEMessage) message = gsa2_soe_message_code_send(self->base, identity);
    (void)gsa2_soe_session_send_message(self, message);
    GDateTime *ret = gsa2_soe_message_code_send_finish(message, error);
    return ret;
}

GSA2Account *gsa2_soe_session_account_create_sync(GSA2SOESession *self, GSA2Account *account, GList *identities, GDateTime **expiration, GError **error) {
    g_autoptr(GSA2SOEMessage) message = gsa2_soe_message_account_create(self->base, account, identities);
    (void)gsa2_soe_session_send_message(self, message);
    GSA2Account *ret = gsa2_soe_message_account_create_finish(message, self->connection, expiration, error);
    return ret;
}

GSA2Account *gsa2_soe_session_account_get_sync(GSA2SOESession *self, gchar *access, GError **error) {
    g_autoptr(GSA2SOEMessage) message = gsa2_soe_message_account_get(self->base, access);
    (void)gsa2_soe_session_send_message(self, message);
    GSA2Account *ret = gsa2_soe_message_account_get_finish(message, self->connection, error);
    return ret;
}

gboolean gsa2_soe_session_password_update_sync(GSA2SOESession *self, gchar *password, gchar *access, GError **error) {
    g_autoptr(GSA2SOEMessage) message = gsa2_soe_message_password_update(self->base, password, access);
    (void)gsa2_soe_session_send_message(self, message);
    gboolean ret = gsa2_soe_message_password_update_finish(message, error);
    return ret;
}

GSA2DomainFull *gsa2_soe_session_domain_create_sync(GSA2SOESession *self, GSA2Domain *domain, GList *controllers_full, gchar *access, GError **error) {
    g_autoptr(GSA2SOEMessage) message = gsa2_soe_message_domain_create(self->base, domain, controllers_full, access);
    (void)gsa2_soe_session_send_message(self, message);
    GSA2DomainFull *ret = gsa2_soe_message_domain_create_finish(message, self->connection, error);
    return ret;
}

gboolean gsa2_soe_session_domain_rename_sync(GSA2SOESession *self, gchar *domain_id, gchar *name, gchar *access, GError **error) {
    g_autoptr(GSA2SOEMessage) message = gsa2_soe_message_domain_rename(self->base, domain_id, name, access);
    (void)gsa2_soe_session_send_message(self, message);
    gboolean ret = gsa2_soe_message_domain_rename_finish(message, self->connection, error);
    return ret;
}

gboolean gsa2_soe_session_domain_delete_sync(GSA2SOESession *self, gchar *domain_id, gchar *access, GError **error) {
    g_autoptr(GSA2SOEMessage) message = gsa2_soe_message_domain_delete(self->base, domain_id, access);
    (void)gsa2_soe_session_send_message(self, message);
    gboolean ret = gsa2_soe_message_domain_delete_finish(message, self->connection, error);
    return ret;
}

gboolean gsa2_soe_session_domain_exit_sync(GSA2SOESession *self, gchar *domain_id, gchar *access, GError **error) {
    g_autoptr(GSA2SOEMessage) message = gsa2_soe_message_domain_exit(self->base, domain_id, access);
    (void)gsa2_soe_session_send_message(self, message);
    gboolean ret = gsa2_soe_message_domain_exit_finish(message, self->connection, error);
    return ret;
}

GSA2DomainFull *gsa2_soe_session_guest_delete_sync(GSA2SOESession *self, gchar *guest_id, gchar *domain_id, gchar *access, GError **error) {
    g_autoptr(GSA2SOEMessage) message = gsa2_soe_message_guest_delete(self->base, guest_id, domain_id, access);
    (void)gsa2_soe_session_send_message(self, message);
    GSA2DomainFull *ret = gsa2_soe_message_guest_delete_finish(message, self->connection, error);
    return ret;
}

GSA2Invitation *gsa2_soe_session_invitation_create_sync(GSA2SOESession *self, GSA2Invitation *invitation, gchar *domain_id, gchar *access, GError **error) {
    g_autoptr(GSA2SOEMessage) message = gsa2_soe_message_invitation_create(self->base, invitation, domain_id, access);
    (void)gsa2_soe_session_send_message(self, message);
    GSA2Invitation *ret = gsa2_soe_message_invitation_create_finish(message, self->connection, error);
    return ret;
}

gboolean gsa2_soe_session_invitation_revoke_sync(GSA2SOESession *self, gchar *invitation_id, gchar *domain_id, gchar *access, GError **error) {
    g_autoptr(GSA2SOEMessage) message = gsa2_soe_message_invitation_revoke(self->base, invitation_id, domain_id, access);
    (void)gsa2_soe_session_send_message(self, message);
    gboolean ret = gsa2_soe_message_invitation_revoke_finish(message, self->connection, error);
    return ret;
}

GSA2DomainFull *gsa2_soe_session_invitation_accept_sync(GSA2SOESession *self, gchar *invitation_id, gchar *access, GError **error) {
    g_autoptr(GSA2SOEMessage) message = gsa2_soe_message_invitation_accept(self->base, invitation_id, access);
    (void)gsa2_soe_session_send_message(self, message);
    GSA2DomainFull *ret = gsa2_soe_message_invitation_accept_finish(message, self->connection, error);
    return ret;
}

gchar *gsa2_soe_session_code_get_sync(GSA2SOESession *self, gchar *domain_id, gchar *access, GError **error) {
    g_autoptr(GSA2SOEMessage) message = gsa2_soe_message_code_get(self->base, domain_id, access);
    (void)gsa2_soe_session_send_message(self, message);
    gchar *ret = gsa2_soe_message_code_get_finish(message, error);
    return ret;
}

gboolean gsa2_soe_session_controllers_add_sync(GSA2SOESession *self, GList *controllers_full, gchar *domain_id, gchar *access, GList **out_controllers_full, GError **error) {
    g_autoptr(GSA2SOEMessage) message = gsa2_soe_message_controllers_add(self->base, controllers_full, domain_id, access);
    (void)gsa2_soe_session_send_message(self, message);
    gboolean ret = gsa2_soe_message_controllers_add_finish(message, self->connection, out_controllers_full, error);
    return ret;
}

gboolean gsa2_soe_session_controller_remove_sync(GSA2SOESession *self, gchar *controller_id, gchar *domain_id, gchar *access, GError **error) {
    g_autoptr(GSA2SOEMessage) message = gsa2_soe_message_controller_remove(self->base, controller_id, domain_id, access);
    (void)gsa2_soe_session_send_message(self, message);
    gboolean ret = gsa2_soe_message_controller_remove_finish(message, self->connection, error);
    return ret;
}

GSA2Controller *gsa2_soe_session_controller_create_sync(GSA2SOESession *self, GSA2Controller *controller, GError **error) {
    g_autoptr(GSA2SOEMessage) message = gsa2_soe_message_controller_create(self->base, controller);
    (void)gsa2_soe_session_send_message(self, message);
    GSA2Controller *ret = gsa2_soe_message_controller_create_finish(message, self->connection, error);
    return ret;
}

gboolean gsa2_soe_session_controller_verify_sync(GSA2SOESession *self, gchar *code, gchar *access, GError **error) {
    g_autoptr(GSA2SOEMessage) message = gsa2_soe_message_controller_verify(self->base, code, access);
    (void)gsa2_soe_session_send_message(self, message);
    gboolean ret = gsa2_soe_message_controller_verify_finish(message, self->connection, error);
    return ret;
}

GSA2Token *gsa2_soe_session_token_issue_sync(GSA2SOESession *self, GSA2Identity *identity, GSA2Credential *credential, GError **error) {
    g_autoptr(GSA2SOEMessage) message = gsa2_soe_message_token_issue(self->base, identity, credential);
    (void)gsa2_soe_session_send_message(self, message);
    GSA2Token *ret = gsa2_soe_message_token_issue_finish(message, self->connection, error);
    return ret;
}

GSA2Token *gsa2_soe_session_token_refresh_sync(GSA2SOESession *self, gchar *refresh, GError **error) {
    g_autoptr(GSA2SOEMessage) message = gsa2_soe_message_token_refresh(self->base, refresh);
    (void)gsa2_soe_session_send_message(self, message);
    GSA2Token *ret = gsa2_soe_message_token_refresh_finish(message, self->connection, error);
    return ret;
}

gboolean gsa2_soe_session_token_revoke_sync(GSA2SOESession *self, gchar *access, GError **error) {
    g_autoptr(GSA2SOEMessage) message = gsa2_soe_message_token_revoke(self->base, access);
    (void)gsa2_soe_session_send_message(self, message);
    gboolean ret = gsa2_soe_message_token_revoke_finish(message, self->connection, error);
    return ret;
}

gchar *gsa2_soe_session_camera_playlist_sync(GSA2SOESession *self, gchar *id, gchar *key, gchar *controller, gchar *access, GError **error) {
    g_autoptr(GSA2SOEMessage) message = gsa2_soe_message_camera_playlist(self->base, id, key, controller, access);
    (void)gsa2_soe_session_send_message(self, message);
    gchar *ret = gsa2_soe_message_camera_playlist_finish(message, error);
    return ret;
}

gboolean gsa2_soe_session_camera_heartbeat_sync(GSA2SOESession *self, gchar *id, gchar *controller, gchar *access, GError **error) {
    g_autoptr(GSA2SOEMessage) message = gsa2_soe_message_camera_heartbeat(self->base, id, controller, access);
    (void)gsa2_soe_session_send_message(self, message);
    gboolean ret = gsa2_soe_message_camera_heartbeat_finish(message, error);
    return ret;
}
