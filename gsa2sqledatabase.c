//
// Created by Dan on 23.02.2025.
//

#include "gsa2sqledatabase.h"

gboolean gsa2_sqle_database_set(gchar *file, GError **error) {
    if (g_file_test(file, G_FILE_TEST_EXISTS)) return TRUE;
    g_autofree gchar *directory = g_path_get_dirname(file);
    if (ge_mkdir_with_parents(directory, 0700, error) == -1) return FALSE;
    if (!g_file_set_contents(file, gsa2_sqle_database_data, gsa2_sqle_database_n, error)) return FALSE;
    return TRUE;
}
