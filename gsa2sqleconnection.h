//
// Created by root on 27.05.2020.
//

#ifndef LIBRARY_GS_AUTH_2_GSA2SQLECONNECTION_H
#define LIBRARY_GS_AUTH_2_GSA2SQLECONNECTION_H

#include "gsa2main.h"
#include "gsa2schema.h"
#include "gsa2sqlestatement.h"
#include "gsa2sqledatabase.h"

G_BEGIN_DECLS

#define GSA2_TYPE_SQLE_CONNECTION gsa2_sqle_connection_get_type()

GE_DECLARE_DERIVABLE_TYPE(GSA2SQLEConnection, gsa2_sqle_connection, GSA2, SQLE_CONNECTION, SQLEConnection)

struct _GSA2SQLEConnectionClass {
    SQLEConnectionClass super;

    void (*account_deleting)(GSA2SQLEConnection *self, GSA2Account *account);
    void (*account_domain_deleting)(GSA2SQLEConnection *self, GSA2AccountDomain *account_domain);
    void (*domain_controller_deleting)(GSA2SQLEConnection *self, GSA2DomainController *domain_controller);
};

struct _GSA2SQLEConnection {
    SQLEConnection super;
};

void gsa2_sqle_connection_account_deleting(GSA2SQLEConnection *self, GSA2Account *account);
void gsa2_sqle_connection_account_domain_deleting(GSA2SQLEConnection *self, GSA2AccountDomain *account_domain);
void gsa2_sqle_connection_domain_controller_deleting(GSA2SQLEConnection *self, GSA2DomainController *domain_controller);

GSA2SQLEConnection *gsa2_sqle_connection_new(gchar *file, GError **error);

gint gsa2_sqle_connection_account_upsert(GSA2SQLEConnection *self, GSA2Account *account, GError **error);
gint gsa2_sqle_connection_account_delete(GSA2SQLEConnection *self, gchar *tail, GError **error);
gint gsa2_sqle_connection_account_select(GSA2SQLEConnection *self, gchar *tail, GList **accounts, GError **error);
gint gsa2_sqle_connection_account_preupdate(GSA2SQLEConnection *self, gboolean new, GSA2Account **account, GError **error);

gint gsa2_sqle_connection_domain_upsert(GSA2SQLEConnection *self, GSA2Domain *domain, GError **error);
gint gsa2_sqle_connection_domain_update_name_by_id(GSA2SQLEConnection *self, gchar *id, gchar *name, GError **error);
gint gsa2_sqle_connection_domain_select(GSA2SQLEConnection *self, gchar *tail, GList **domains, GError **error);

gint gsa2_sqle_connection_controller_upsert(GSA2SQLEConnection *self, GSA2Controller *controller, GError **error);
gint gsa2_sqle_connection_controller_select(GSA2SQLEConnection *self, gchar *tail, GList **controllers, GError **error);
gint gsa2_sqle_connection_controller_select_by_mac(GSA2SQLEConnection *self, gchar *mac, GList **controllers, GError **error);

gint gsa2_sqle_connection_account_domain_upsert(GSA2SQLEConnection *self, GSA2AccountDomain *account_domain, GError **error);
gint gsa2_sqle_connection_account_domain_delete_by_account_and_domain(GSA2SQLEConnection *self, gchar *account, gchar *domain, GError **error);
gint gsa2_sqle_connection_account_domain_delete_by_account_and_domains(GSA2SQLEConnection *self, gchar *account, GPtrArray *domains, GError **error);
gint gsa2_sqle_connection_account_domain_delete_by_domain_and_accounts(GSA2SQLEConnection *self, gchar *domain, GPtrArray *accounts, GError **error);
gint gsa2_sqle_connection_account_domain_select(GSA2SQLEConnection *self, gchar *tail, GList **account_domains, GError **error);
gint gsa2_sqle_connection_account_domain_preupdate(GSA2SQLEConnection *self, gboolean new, GSA2AccountDomain **account_domain, GError **error);

gint gsa2_sqle_connection_domain_controller_upsert(GSA2SQLEConnection *self, GSA2DomainController *domain_controller, GError **error);
gint gsa2_sqle_connection_domain_controller_update_verified_by_controller(GSA2SQLEConnection *self, gchar *controller, gint verified, GError **error);
gint gsa2_sqle_connection_domain_controller_delete_by_domain_and_controller(GSA2SQLEConnection *self, gchar *domain, gchar *controller, GError **error);
gint gsa2_sqle_connection_domain_controller_delete_by_domain_and_controllers(GSA2SQLEConnection *self, gchar *domain, GPtrArray *controllers, GError **error);
gint gsa2_sqle_connection_domain_controller_select(GSA2SQLEConnection *self, gchar *tail, GList **domain_controllers, GError **error);
gint gsa2_sqle_connection_domain_controller_preupdate(GSA2SQLEConnection *self, gboolean new, GSA2DomainController **domain_controller, GError **error);

gint gsa2_sqle_connection_identity_upsert(GSA2SQLEConnection *self, GSA2Identity *identity, GError **error);
gint gsa2_sqle_connection_identity_select(GSA2SQLEConnection *self, gchar *tail, GList **identities, GError **error);

gint gsa2_sqle_connection_invitation_upsert(GSA2SQLEConnection *self, GSA2Invitation *invitation, GError **error);
gint gsa2_sqle_connection_invitation_delete_by_domain_and_id(GSA2SQLEConnection *self, gchar *domain, gchar *id, GError **error);
gint gsa2_sqle_connection_invitation_delete_by_domain_and_ids(GSA2SQLEConnection *self, gchar *domain, GPtrArray *ids, GError **error);
gint gsa2_sqle_connection_invitation_select(GSA2SQLEConnection *self, gchar *tail, GList **invitations, GError **error);

gint gsa2_sqle_connection_token_upsert(GSA2SQLEConnection *self, GSA2Token *token, GError **error);
gint gsa2_sqle_connection_token_delete_by_access(GSA2SQLEConnection *self, gchar *access, GError **error);
gint gsa2_sqle_connection_token_select(GSA2SQLEConnection *self, gchar *tail, GList **tokens, GError **error);
gint gsa2_sqle_connection_token_select_by_access(GSA2SQLEConnection *self, gchar *access, GList **tokens, GError **error);

gint gsa2_sqle_connection_account_full_select(GSA2SQLEConnection *self, gchar *tail, GList **accounts_full, GError **error);

gint gsa2_sqle_connection_domain_full_select(GSA2SQLEConnection *self, gchar *tail, GList **domains_full, GError **error);

gint gsa2_sqle_connection_controller_full_select(GSA2SQLEConnection *self, gchar *tail, GList **controllers_full, GError **error);

G_END_DECLS

#endif //LIBRARY_GS_AUTH_2_GSA2SQLECONNECTION_H
