//
// Created by root on 05.06.2020.
//

#include "gsa2jsereader.h"

gboolean gsa2_jse_reader_identity_check(JsonObject *self) {
    gboolean ret = json_object_get_boolean_member(self, "available");
    return ret;
}

GDateTime *gsa2_jse_reader_code_send(JsonObject *self) {
    GDateTime *ret = gsa2_jse_reader_expiration(self);
    return ret;
}

GSA2Account *gsa2_jse_reader_account_create(JsonObject *self, GSA2SQLEConnection *connection, GDateTime **expiration, GError **error) {
    g_autoptr(SQLEAutoTransaction) transaction = NULL;
    if ((transaction = sqle_transaction_new(SQLE_CONNECTION(connection)->object, error)) == NULL) return NULL;

    JsonObject *object_account = json_object_get_object_member(self, "account");
    g_autoptr(GSA2Account) ret = NULL;
    if ((ret = gsa2_jse_reader_account(object_account, connection, error)) == NULL) return NULL;

    JsonObject *object_expiration = json_object_get_object_member(self, "activationDetails");
    g_autoptr(GDateTime) ret_expiration = gsa2_jse_reader_expiration(object_expiration);

    if (sqle_auto_transaction_commit(&transaction, error) != SQLITE_OK) return NULL;

    GE_SET_VALUE(expiration, g_steal_pointer(&ret_expiration));
    return g_steal_pointer(&ret);
}

GSA2Account *gsa2_jse_reader_account_get(JsonObject *self, GSA2SQLEConnection *connection, GError **error) {
    g_autoptr(SQLEAutoTransaction) transaction = NULL;
    if ((transaction = sqle_transaction_new(SQLE_CONNECTION(connection)->object, error)) == NULL) return NULL;

    g_autoptr(GSA2Account) ret = NULL;
    if ((ret = gsa2_jse_reader_account(self, connection, error)) == NULL) return NULL;

    if (sqle_auto_transaction_commit(&transaction, error) != SQLITE_OK) return NULL;

    return g_steal_pointer(&ret);
}

void gsa2_jse_reader_password_update(JsonObject *self) {

}

GSA2DomainFull *gsa2_jse_reader_domain_create(JsonObject *self, GSA2SQLEConnection *connection, gchar *account_id, GError **error) {
    g_autoptr(SQLEAutoTransaction) transaction = NULL;
    if ((transaction = sqle_transaction_new(SQLE_CONNECTION(connection)->object, error)) == NULL) return NULL;

    g_autoptr(GSA2DomainFull) ret = NULL;
    if ((ret = gsa2_jse_reader_domain_full(self, connection, account_id, error)) == NULL) return NULL;

    if (sqle_auto_transaction_commit(&transaction, error) != SQLITE_OK) return NULL;

    return g_steal_pointer(&ret);
}

gboolean gsa2_jse_reader_domain_rename(JsonObject *self, GSA2SQLEConnection *connection, gchar *domain_id, GError **error) {
    g_autoptr(SQLEAutoTransaction) transaction = NULL;
    if ((transaction = sqle_transaction_new(SQLE_CONNECTION(connection)->object, error)) == NULL) return FALSE;

    gchar *name = (gchar *)json_object_get_string_member(self, "name");

    if (gsa2_sqle_connection_domain_update_name_by_id(connection, domain_id, name, error) != SQLITE_DONE) return FALSE;

    if (sqle_auto_transaction_commit(&transaction, error) != SQLITE_OK) return FALSE;

    return TRUE;
}

gboolean gsa2_jse_reader_domain_delete(JsonObject *self, GSA2SQLEConnection *connection, gchar *account_id, gchar *domain_id, GError **error) {
    g_autoptr(SQLEAutoTransaction) transaction = NULL;
    if ((transaction = sqle_transaction_new(SQLE_CONNECTION(connection)->object, error)) == NULL) return FALSE;

    if (gsa2_sqle_connection_account_domain_delete_by_account_and_domain(connection, account_id, domain_id, error) != SQLITE_DONE) return FALSE;

    if (sqle_auto_transaction_commit(&transaction, error) != SQLITE_OK) return FALSE;

    return TRUE;
}

gboolean gsa2_jse_reader_domain_exit(JsonObject *self, GSA2SQLEConnection *connection, gchar *account_id, gchar *domain_id, GError **error) {
    g_autoptr(SQLEAutoTransaction) transaction = NULL;
    if ((transaction = sqle_transaction_new(SQLE_CONNECTION(connection)->object, error)) == NULL) return FALSE;

    if (gsa2_sqle_connection_account_domain_delete_by_account_and_domain(connection, account_id, domain_id, error) != SQLITE_DONE) return FALSE;

    if (sqle_auto_transaction_commit(&transaction, error) != SQLITE_OK) return FALSE;

    return TRUE;
}

GSA2DomainFull *gsa2_jse_reader_guest_delete(JsonObject *self, GSA2SQLEConnection *connection, gchar *account_id, GError **error) {
    g_autoptr(SQLEAutoTransaction) transaction = NULL;
    if ((transaction = sqle_transaction_new(SQLE_CONNECTION(connection)->object, error)) == NULL) return NULL;

    JsonObject *object_domain_full = json_object_get_object_member(self, "domain");
    g_autoptr(GSA2DomainFull) ret = NULL;
    if ((ret = gsa2_jse_reader_domain_full(object_domain_full, connection, account_id, error)) == NULL) return NULL;

    if (sqle_auto_transaction_commit(&transaction, error) != SQLITE_OK) return NULL;

    return g_steal_pointer(&ret);
}

GSA2Invitation *gsa2_jse_reader_invitation_create(JsonObject *self, GSA2SQLEConnection *connection, gchar *domain_id, GError **error) {
    g_autoptr(SQLEAutoTransaction) transaction = NULL;
    if ((transaction = sqle_transaction_new(SQLE_CONNECTION(connection)->object, error)) == NULL) return NULL;

    g_autoptr(GSA2Invitation) ret = NULL;
    if ((ret = gsa2_jse_reader_invitation(self, connection, domain_id, error)) == NULL) return NULL;

    if (sqle_auto_transaction_commit(&transaction, error) != SQLITE_OK) return NULL;

    return g_steal_pointer(&ret);
}

gboolean gsa2_jse_reader_invitation_revoke(JsonObject *self, GSA2SQLEConnection *connection, gchar *domain_id, gchar *invitation_id, GError **error) {
    g_autoptr(SQLEAutoTransaction) transaction = NULL;
    if ((transaction = sqle_transaction_new(SQLE_CONNECTION(connection)->object, error)) == NULL) return FALSE;

    if (gsa2_sqle_connection_invitation_delete_by_domain_and_id(connection, domain_id, invitation_id, error) != SQLITE_DONE) return FALSE;

    if (sqle_auto_transaction_commit(&transaction, error) != SQLITE_OK) return FALSE;

    return TRUE;
}

GSA2DomainFull *gsa2_jse_reader_invitation_accept(JsonObject *self, GSA2SQLEConnection *connection, gchar *account_id, GError **error) {
    g_autoptr(SQLEAutoTransaction) transaction = NULL;
    if ((transaction = sqle_transaction_new(SQLE_CONNECTION(connection)->object, error)) == NULL) return NULL;

    g_autoptr(GSA2DomainFull) ret = NULL;
    if ((ret = gsa2_jse_reader_domain_full(self, connection, account_id, error)) == NULL) return NULL;

    if (sqle_auto_transaction_commit(&transaction, error) != SQLITE_OK) return NULL;

    return g_steal_pointer(&ret);
}

gchar *gsa2_jse_reader_code_get(JsonObject *self) {
    gchar *ret = (gchar *)json_object_get_string_member(self, "code");
    return g_strdup(ret);
}

gboolean gsa2_jse_reader_controllers_add(JsonObject *self, GSA2SQLEConnection *connection, gchar *domain_id, GList **controllers_full, GError **error) {
    g_autoptr(SQLEAutoTransaction) transaction = NULL;
    if ((transaction = sqle_transaction_new(SQLE_CONNECTION(connection)->object, error)) == NULL) return FALSE;

    JsonArray *array_controllers_full = json_object_get_array_member(self, "controllers");
    if (!gsa2_jse_reader_controllers_full_v1(array_controllers_full, connection, domain_id, controllers_full, error)) return FALSE;

    if (sqle_auto_transaction_commit(&transaction, error) != SQLITE_OK) return FALSE;

    return TRUE;
}

gboolean gsa2_jse_reader_controller_remove(JsonObject *self, GSA2SQLEConnection *connection, gchar *domain_id, gchar *controller_id, GError **error) {
    g_autoptr(SQLEAutoTransaction) transaction = NULL;
    if ((transaction = sqle_transaction_new(SQLE_CONNECTION(connection)->object, error)) == NULL) return FALSE;

    if (gsa2_sqle_connection_domain_controller_delete_by_domain_and_controller(connection, domain_id, controller_id, error) != SQLITE_DONE) return FALSE;

    if (sqle_auto_transaction_commit(&transaction, error) != SQLITE_OK) return FALSE;

    return TRUE;
}

GSA2Controller *gsa2_jse_reader_controller_create(JsonObject *self, GSA2SQLEConnection *connection, GError **error) {
    g_autoptr(SQLEAutoTransaction) transaction = NULL;
    if ((transaction = sqle_transaction_new(SQLE_CONNECTION(connection)->object, error)) == NULL) return NULL;

    g_autoptr(GSA2Controller) ret = NULL;
    if ((ret = gsa2_jse_reader_controller(self, connection, error)) == NULL) return NULL;

    if (sqle_auto_transaction_commit(&transaction, error) != SQLITE_OK) return NULL;

    return g_steal_pointer(&ret);
}

gboolean gsa2_jse_reader_controller_verify(JsonObject *self, GSA2SQLEConnection *connection, gchar *controller_id, GError **error) {
    g_autoptr(SQLEAutoTransaction) transaction = NULL;
    if ((transaction = sqle_transaction_new(SQLE_CONNECTION(connection)->object, error)) == NULL) return FALSE;
    
    if (gsa2_sqle_connection_domain_controller_update_verified_by_controller(connection, controller_id, TRUE, error) != SQLITE_DONE) return FALSE;
    
    if (sqle_auto_transaction_commit(&transaction, error) != SQLITE_OK) return FALSE;

    return TRUE;
}

GSA2Token *gsa2_jse_reader_token_issue(JsonObject *self, GSA2SQLEConnection *connection, GError **error) {
    g_autoptr(SQLEAutoTransaction) transaction = NULL;
    if ((transaction = sqle_transaction_new(SQLE_CONNECTION(connection)->object, error)) == NULL) return NULL;

    g_autoptr(GSA2Token) ret = NULL;
    if ((ret = gsa2_jse_reader_token(self, connection, error)) == NULL) return NULL;

    if (sqle_auto_transaction_commit(&transaction, error) != SQLITE_OK) return NULL;

    return g_steal_pointer(&ret);
}

GSA2Token *gsa2_jse_reader_token_refresh(JsonObject *self, GSA2SQLEConnection *connection, GError **error) {
    g_autoptr(SQLEAutoTransaction) transaction = NULL;
    if ((transaction = sqle_transaction_new(SQLE_CONNECTION(connection)->object, error)) == NULL) return NULL;

    g_autoptr(GSA2Token) ret = NULL;
    if ((ret = gsa2_jse_reader_token(self, connection, error)) == NULL) return NULL;

    if (sqle_auto_transaction_commit(&transaction, error) != SQLITE_OK) return NULL;

    return g_steal_pointer(&ret);
}

gboolean gsa2_jse_reader_token_revoke(JsonObject *self, GSA2SQLEConnection *connection, GError **error) {
    g_autoptr(SQLEAutoTransaction) transaction = NULL;
    if ((transaction = sqle_transaction_new(SQLE_CONNECTION(connection)->object, error)) == NULL) return FALSE;

    gchar *access = (gchar *)json_object_get_string_member(self, "accessToken");
    if (gsa2_sqle_connection_token_delete_by_access(connection, access, error) != SQLITE_DONE) return FALSE;

    if (sqle_auto_transaction_commit(&transaction, error) != SQLITE_OK) return FALSE;

    return TRUE;
}

gchar *gsa2_jse_reader_camera_playlist(JsonObject *self) {
    gchar *ret = (gchar *)json_object_get_string_member(self, "playlist_url");
    return g_strdup(ret);
}

GSA2Account *gsa2_jse_reader_account(JsonObject *self, GSA2SQLEConnection *connection, GError **error) {
    JsonObject *object_credentials = json_object_get_object_member(self, "credentials");

    GSA2Account ret = {0};
    ret.id = (gchar *)json_object_get_string_member(self, "id");
    ret.name = (gchar *)json_object_get_string_member(self, "name");
    ret.password = (gchar *)json_object_get_string_member(object_credentials, "password");

    if (gsa2_sqle_connection_account_upsert(connection, &ret, error) != SQLITE_DONE) return NULL;

    JsonArray *array_identities = json_object_get_array_member(object_credentials, "identities");
    if (!gsa2_jse_reader_identities(array_identities, connection, ret.id, NULL, error)) return NULL;

    JsonArray *array_domains_full = json_object_get_array_member(self, "domains");
    if (!gsa2_jse_reader_domains_full(array_domains_full, connection, ret.id, NULL, error)) return NULL;

    return gsa2_account_copy(&ret);
}

GSA2AccountFull *gsa2_jse_reader_owner_full(JsonObject *self, GSA2SQLEConnection *connection, gchar *account_id, gchar *domain_id, GError **error) {
    JsonObject *object_credentials = json_object_get_object_member(self, "credentials");

    GSA2Account account = {0};
    account.id = (gchar *)json_object_get_string_member(self, "id");
    account.name = (gchar *)json_object_get_string_member(self, "name");
    account.password = (gchar *)json_object_get_string_member(object_credentials, "password");

    if (gsa2_sqle_connection_account_upsert(connection, &account, error) != SQLITE_DONE) return NULL;

    GSA2AccountDomain account_domain = {0};
    account_domain.account = account.id;
    account_domain.domain = domain_id;
    account_domain.current = g_str_equal(account.id, account_id);

    if (gsa2_sqle_connection_account_domain_upsert(connection, &account_domain, error) != SQLITE_DONE) return NULL;

    JsonArray *array_identities = json_object_get_array_member(object_credentials, "identities");
    if (!gsa2_jse_reader_identities(array_identities, connection, account.id, NULL, error)) return NULL;

    GSA2AccountFull ret = {0};
    ret.account = &account;
    ret.account_domain = &account_domain;
    return gsa2_account_full_copy(&ret);
}

GSA2AccountFull *gsa2_jse_reader_guest_full(JsonObject *self, GSA2SQLEConnection *connection, gchar *account_id, gchar *domain_id, GError **error) {
    GSA2Account account = {0};
    account.id = (gchar *)json_object_get_string_member(self, "id");
    account.name = (gchar *)json_object_get_string_member(self, "name");
    account.password = "";

    if (gsa2_sqle_connection_account_upsert(connection, &account, error) != SQLITE_DONE) return NULL;

    GSA2AccountDomain account_domain = {0};
    account_domain.account = account.id;
    account_domain.domain = domain_id;
    account_domain.expiration = (gchar *)json_object_get_string_member(self, "accessExpiresAt");
    account_domain.current = g_str_equal(account.id, account_id);

    if (gsa2_sqle_connection_account_domain_upsert(connection, &account_domain, error) != SQLITE_DONE) return NULL;

    GSA2AccountFull ret = {0};
    ret.account = &account;
    ret.account_domain = &account_domain;
    return gsa2_account_full_copy(&ret);
}

gboolean gsa2_jse_reader_guests_full(JsonArray *self, GSA2SQLEConnection *connection, gchar *account_id, gchar *domain_id, GList **guests_full, GError **error) {
    g_autolist(GSA2AccountFull) ret_guests_full = NULL;
    guint length = json_array_get_length(self);

    g_autoptr(GPtrArray) ids = g_ptr_array_new();

    for (guint index = 0; index < length; index++) {
        GSA2AccountFull *ret_guest_full = NULL;
        JsonObject *object_guest_full = json_array_get_object_element(self, index);
        if ((ret_guest_full = gsa2_jse_reader_guest_full(object_guest_full, connection, account_id, domain_id, error)) == NULL) return FALSE;
        ret_guests_full = g_list_append(ret_guests_full, ret_guest_full);

        g_ptr_array_add(ids, ret_guest_full->account->id);
    }

    if (gsa2_sqle_connection_account_domain_delete_by_domain_and_accounts(connection, domain_id, ids, error) != SQLITE_DONE) return FALSE;

    GE_SET_VALUE(guests_full, g_steal_pointer(&ret_guests_full));
    return TRUE;
}

GSA2DomainFull *gsa2_jse_reader_domain_full(JsonObject *self, GSA2SQLEConnection *connection, gchar *account_id, GError **error) {
    JsonObject *object_owner_full = json_object_get_object_member(self, "owner");

    gchar *domain_id = (gchar *)json_object_get_string_member(self, "id");

    g_autoptr(GSA2AccountFull) owner_full = NULL;
    if ((owner_full = gsa2_jse_reader_owner_full(object_owner_full, connection, account_id, domain_id, error)) == NULL) return NULL;

    GSA2Domain domain = {0};
    domain.id = domain_id;
    domain.owner = owner_full->account->id;
    domain.name = (gchar *)json_object_get_string_member(self, "name");

    if (gsa2_sqle_connection_domain_upsert(connection, &domain, error) != SQLITE_DONE) return NULL;

    GSA2AccountDomain account_domain = {0};
    account_domain.account = account_id;
    account_domain.domain = domain_id;
    account_domain.current = TRUE;

    if (gsa2_sqle_connection_account_domain_upsert(connection, &account_domain, error) != SQLITE_DONE) return NULL;

    JsonArray *array_controllers_full = json_object_get_array_member(self, "controllers");
    if (!gsa2_jse_reader_controllers_full(array_controllers_full, connection, domain_id, NULL, error)) return NULL;

    JsonArray *array_guests_full = json_object_get_array_member(self, "guests");
    if (!gsa2_jse_reader_guests_full(array_guests_full, connection, account_id, domain_id, NULL, error)) return NULL;

    JsonArray *array_invitations = json_object_get_array_member(self, "invitations");
    if (!gsa2_jse_reader_invitations(array_invitations, connection, domain_id, NULL, error)) return NULL;

    GSA2DomainFull ret = {0};
    ret.domain = &domain;
    ret.account_domain = &account_domain;
    return gsa2_domain_full_copy(&ret);
}

gboolean gsa2_jse_reader_domains_full(JsonArray *self, GSA2SQLEConnection *connection, gchar *account_id, GList **domains_full, GError **error) {
    g_autolist(GSA2DomainFull) ret_domains_full = NULL;
    guint length = json_array_get_length(self);

    g_autoptr(GPtrArray) ids = g_ptr_array_new();

    for (guint index = 0; index < length; index++) {
        GSA2DomainFull *ret_domain_full = NULL;
        JsonObject *object_domain_full = json_array_get_object_element(self, index);
        if ((ret_domain_full = gsa2_jse_reader_domain_full(object_domain_full, connection, account_id, error)) == NULL) return FALSE;
        ret_domains_full = g_list_append(ret_domains_full, ret_domain_full);

        g_ptr_array_add(ids, ret_domain_full->domain->id);
    }

    if (gsa2_sqle_connection_account_domain_delete_by_account_and_domains(connection, account_id, ids, error) != SQLITE_DONE) return FALSE;

    GE_SET_VALUE(domains_full, g_steal_pointer(&ret_domains_full));
    return TRUE;
}

GSA2Controller *gsa2_jse_reader_controller(JsonObject *self, GSA2SQLEConnection *connection, GError **error) {
    GSA2Controller ret = {0};
    ret.id = (gchar *)json_object_get_string_member(self, "id");
    ret.mac = (gchar *)json_object_get_string_member(self, "macAddress");
    ret.model = (gchar *)json_object_get_string_member(self, "model");
    ret.serial = (gchar *)json_object_get_string_member(self, "serial");

    if (gsa2_sqle_connection_controller_upsert(connection, &ret, error) != SQLITE_DONE) return NULL;

    return gsa2_controller_copy(&ret);
}

GSA2ControllerFull *gsa2_jse_reader_controller_full(JsonObject *self, GSA2SQLEConnection *connection, gchar *domain_id, GError **error) {
    GSA2Controller controller = {0};
    controller.id = (gchar *)json_object_get_string_member(self, "id");
    controller.mac = (gchar *)json_object_get_string_member(self, "macAddress");
    controller.model = (gchar *)json_object_get_string_member(self, "model");
    controller.serial = (gchar *)json_object_get_string_member(self, "serial");

    if (gsa2_sqle_connection_controller_upsert(connection, &controller, error) != SQLITE_DONE) return NULL;

    GSA2DomainController domain_controller = {0};
    domain_controller.domain = domain_id;
    domain_controller.controller = controller.id;
    domain_controller.key = (gchar *)json_object_get_string_member(self, "localAccessKey");
    domain_controller.ip = (gchar *)json_object_get_string_member(self, "ip");
    domain_controller.port = (gint)json_object_get_int_member(self, "port");
    domain_controller.bssid = (gchar *)json_object_get_string_member(self, "bssid");
    domain_controller.verified = json_object_get_boolean_member(self, "verified");

    if (gsa2_sqle_connection_domain_controller_upsert(connection, &domain_controller, error) != SQLITE_DONE) return NULL;

    GSA2ControllerFull ret = {0};
    ret.controller = &controller;
    ret.domain_controller = &domain_controller;
    return gsa2_controller_full_copy(&ret);
}

gboolean gsa2_jse_reader_controllers_full(JsonArray *self, GSA2SQLEConnection *connection, gchar *domain_id, GList **controllers_full, GError **error) {
    g_autolist(GSA2ControllerFull) ret_controllers_full = NULL;
    guint length = json_array_get_length(self);

    g_autoptr(GPtrArray) ids = g_ptr_array_new();

    for (guint index = 0; index < length; index++) {
        GSA2ControllerFull *ret_controller_full = NULL;
        JsonObject *object_controller_full = json_array_get_object_element(self, index);
        if ((ret_controller_full = gsa2_jse_reader_controller_full(object_controller_full, connection, domain_id, error)) == NULL) return FALSE;
        ret_controllers_full = g_list_append(ret_controllers_full, ret_controller_full);

        g_ptr_array_add(ids, ret_controller_full->controller->id);
    }

    if (gsa2_sqle_connection_domain_controller_delete_by_domain_and_controllers(connection, domain_id, ids, error) != SQLITE_DONE) return FALSE;

    GE_SET_VALUE(controllers_full, g_steal_pointer(&ret_controllers_full));
    return TRUE;
}

gboolean gsa2_jse_reader_controllers_full_v1(JsonArray *self, GSA2SQLEConnection *connection, gchar *domain_id, GList **controllers_full, GError **error) {
    g_autolist(GSA2ControllerFull) ret_controllers_full = NULL;
    guint length = json_array_get_length(self);
    
    for (guint index = 0; index < length; index++) {
        GSA2ControllerFull *ret_controller_full = NULL;
        JsonObject *object_controller_full = json_array_get_object_element(self, index);
        if ((ret_controller_full = gsa2_jse_reader_controller_full(object_controller_full, connection, domain_id, error)) == NULL) return FALSE;
        ret_controllers_full = g_list_append(ret_controllers_full, ret_controller_full);
    }
    
    GE_SET_VALUE(controllers_full, g_steal_pointer(&ret_controllers_full));
    return TRUE;
}

GSA2Identity *gsa2_jse_reader_identity(JsonObject *self, GSA2SQLEConnection *connection, gchar *account_id, GError **error) {
    GSA2Identity ret = {0};
    ret.type = (gchar *)json_object_get_string_member(self, "type");
    ret.value = (gchar *)json_object_get_string_member(self, "value");
    ret.account = account_id;

    if (gsa2_sqle_connection_identity_upsert(connection, &ret, error) != SQLITE_DONE) return NULL;

    return gsa2_identity_copy(&ret);
}

gboolean gsa2_jse_reader_identities(JsonArray *self, GSA2SQLEConnection *connection, gchar *account_id, GList **identities, GError **error) {
    g_autolist(GSA2Identity) ret_identities = NULL;
    guint length = json_array_get_length(self);

    for (guint index = 0; index < length; index++) {
        GSA2Identity *ret_identity = NULL;
        JsonObject *object_identity = json_array_get_object_element(self, index);
        if ((ret_identity = gsa2_jse_reader_identity(object_identity, connection, account_id, error)) == NULL) return FALSE;
        ret_identities = g_list_append(ret_identities, ret_identity);
    }

    GE_SET_VALUE(identities, g_steal_pointer(&ret_identities));
    return TRUE;
}

GSA2Invitation *gsa2_jse_reader_invitation(JsonObject *self, GSA2SQLEConnection *connection, gchar *domain_id, GError **error) {
    GSA2Invitation ret = {0};
    ret.id = (gchar *)json_object_get_string_member(self, "id");
    ret.domain = domain_id;
    ret.name = (gchar *)json_object_get_string_member(self, "name");
    ret.expiration = (gchar *)json_object_get_string_member(self, "expiresAt");
    ret.access = json_object_get_int_member(self, "accessExpiresIn");

    if (gsa2_sqle_connection_invitation_upsert(connection, &ret, error) != SQLITE_DONE) return NULL;

    return gsa2_invitation_copy(&ret);
}

gboolean gsa2_jse_reader_invitations(JsonArray *self, GSA2SQLEConnection *connection, gchar *domain_id, GList **invitations, GError **error) {
    g_autolist(GSA2Invitation) ret_invitations = NULL;
    guint length = json_array_get_length(self);

    g_autoptr(GPtrArray) ids = g_ptr_array_new();

    for (guint index = 0; index < length; index++) {
        GSA2Invitation *ret_invitation = NULL;
        JsonObject *object_invitation = json_array_get_object_element(self, index);
        if ((ret_invitation = gsa2_jse_reader_invitation(object_invitation, connection, domain_id, error)) == NULL) return FALSE;
        ret_invitations = g_list_append(ret_invitations, ret_invitation);

        g_ptr_array_add(ids, ret_invitation->id);
    }

    if (gsa2_sqle_connection_invitation_delete_by_domain_and_ids(connection, domain_id, ids, error) != SQLITE_DONE) return FALSE;

    GE_SET_VALUE(invitations, g_steal_pointer(&ret_invitations));
    return TRUE;
}

GSA2Token *gsa2_jse_reader_token(JsonObject *self, GSA2SQLEConnection *connection, GError **error) {
    gchar *access = (gchar *)json_object_get_string_member(self, "accessToken");

    g_autoptr(JWTToken) token = NULL;
    if ((token = jwt_token_decode(access, error)) == NULL) return NULL;

    GSA2Token ret = {0};
    ret.subject = jwt_payload_get_sub(token->payload);
    ret.id = (gchar *)json_object_get_string_member(token->payload, "id");
    ret.access = access;
    ret.refresh = (gchar *)json_object_get_string_member(self, "refreshToken");

    if (gsa2_sqle_connection_token_upsert(connection, &ret, error) != SQLITE_DONE) return NULL;

    return gsa2_token_copy(&ret);
}

GDateTime *gsa2_jse_reader_expiration(JsonObject *self) {
    gchar *expiration = (gchar *)json_object_get_string_member(self, "expiresAt");

    GDateTime *ret = g_date_time_new_from_iso8601(expiration, NULL);
    return ret;
}

GError *gsa2_jse_reader_error(JsonObject *self) {
    JsonObject *object_object = json_object_get_object_member(self, "object");

    gint64 code = json_object_get_int_member(object_object, "code");
    gchar *message = (gchar *)json_object_get_string_member(object_object, "message");
    gchar *source = (gchar *)json_object_get_string_member(object_object, "source");

    GQuark domain = g_quark_from_string(source);

    GError *ret = g_error_new_literal(domain, (gint)code, message);
    return ret;
}
