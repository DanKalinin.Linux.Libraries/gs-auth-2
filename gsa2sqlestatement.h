//
// Created by root on 31.05.2020.
//

#ifndef LIBRARY_GS_AUTH_2_GSA2SQLESTATEMENT_H
#define LIBRARY_GS_AUTH_2_GSA2SQLESTATEMENT_H

#include "gsa2main.h"
#include "gsa2schema.h"

G_BEGIN_DECLS

gint gsa2_sqle_statement_bind_account(sqlite3_stmt *self, GSA2Account *account, GError **error);
gpointer gsa2_sqle_statement_account(sqlite3_stmt *self);

gint gsa2_sqle_statement_bind_domain(sqlite3_stmt *self, GSA2Domain *domain, GError **error);
gpointer gsa2_sqle_statement_domain(sqlite3_stmt *self);

gint gsa2_sqle_statement_bind_controller(sqlite3_stmt *self, GSA2Controller *controller, GError **error);
gpointer gsa2_sqle_statement_controller(sqlite3_stmt *self);

gint gsa2_sqle_statement_bind_account_domain(sqlite3_stmt *self, GSA2AccountDomain *account_domain, GError **error);
gpointer gsa2_sqle_statement_account_domain(sqlite3_stmt *self);

gint gsa2_sqle_statement_bind_domain_controller(sqlite3_stmt *self, GSA2DomainController *domain_controller, GError **error);
gpointer gsa2_sqle_statement_domain_controller(sqlite3_stmt *self);

gint gsa2_sqle_statement_bind_identity(sqlite3_stmt *self, GSA2Identity *identity, GError **error);
gpointer gsa2_sqle_statement_identity(sqlite3_stmt *self);

gint gsa2_sqle_statement_bind_invitation(sqlite3_stmt *self, GSA2Invitation *invitation, GError **error);
gpointer gsa2_sqle_statement_invitation(sqlite3_stmt *self);

gint gsa2_sqle_statement_bind_token(sqlite3_stmt *self, GSA2Token *token, GError **error);
gpointer gsa2_sqle_statement_token(sqlite3_stmt *self);

gpointer gsa2_sqle_statement_account_full(sqlite3_stmt *self);
gpointer gsa2_sqle_statement_domain_full(sqlite3_stmt *self);
gpointer gsa2_sqle_statement_controller_full(sqlite3_stmt *self);

G_END_DECLS

#endif //LIBRARY_GS_AUTH_2_GSA2SQLESTATEMENT_H
